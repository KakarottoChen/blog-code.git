package com.cc.eed.strategy;

/**
 * <p>简单工厂模式 - 实现的策略模式</p>
 *
 * @author CC
 * @since 2023/10/13
 */
public interface IFactoryStrategy {

    /**
     * 吃饭
     */
    String eating();

    /**
     * 玩
     */
    String play();
}