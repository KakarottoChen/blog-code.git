package com.cc.eed.bean;

import org.springframework.stereotype.Component;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/12
 */
@Component("bean2New")
public class Bean2 {

    public String getBean2() {
        return "Bean2";
    }
}
