package com.cc.eed.singleton;

/**
 * <p>单例模式 - 饿汉 (线程不安全)</p>
 *
 * @author CC
 * @since 2023/10/12
 */
public class SingletonDemo2 {

    private static final SingletonDemo2 INSTANCE = new SingletonDemo2();

    private SingletonDemo2(){
    }

    public static SingletonDemo2 getInstance() {
        return INSTANCE;
    }

}
