package com.cc.eed.enums;

import lombok.Getter;
import org.springframework.util.Assert;

import java.util.Arrays;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/13
 */
@Getter
public enum PeopleEnum {

    MING(1, "小明", "mingSpringBeanImpl"),

    MEI(2, "小美", "meiSpringBeanImpl")

    ;

    public Integer type;

    public String name;

    public String beanName;

    /** <p>根据类型获取beanName<p>
     * @param type type
     * @return {@link String}
     * @since 2023/10/13
     * @author CC
     **/
    public static String getBeanName(Integer type) {
        PeopleEnum peopleEnum = Arrays.stream(values())
                .filter(p -> p.getType().equals(type))
                .findAny().orElse(null);
        Assert.notNull(peopleEnum, "暂不支持的策略模式！");
        return peopleEnum.getBeanName();
    }


    PeopleEnum(Integer type, String name, String beanName) {
        this.type = type;
        this.name = name;
        this.beanName = beanName;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
