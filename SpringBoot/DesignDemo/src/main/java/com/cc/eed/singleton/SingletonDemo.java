package com.cc.eed.singleton;

/**
 * <p>单例模式 - 懒汉 (线程安全)</p>
 * <li>双重验证 + 锁</li>
 *
 * @author CC
 * @since 2023/10/12
 */
public class SingletonDemo {

    private static SingletonDemo INSTANCE;

    private SingletonDemo(){
    }

    public static SingletonDemo getInstance() {
        if (INSTANCE == null) {
            synchronized (SingletonDemo.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SingletonDemo();
                }
            }
        }
        return INSTANCE;
    }

}
