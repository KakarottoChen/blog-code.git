package com.cc.eed;

import com.cc.eed.bean.Bean1;
import com.cc.eed.bean.Bean2;
import com.cc.eed.enums.PeopleEnum;
import com.cc.eed.singleton.SingletonDemo;
import com.cc.eed.singleton.SingletonDemo2;
import com.cc.eed.singleton.SingletonDemo3;
import com.cc.eed.strategy.IFactoryStrategy;
import com.cc.eed.strategy.ISpringBeanStrategy;
import com.cc.eed.strategy.StrategyByFactory;
import com.cc.eed.utils.SpringBeanUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class EasyExcelDemoApplicationTests {

    @Test
    void contextLoads() {
//        System.out.println(SingletonDemo.getInstance());
//        System.out.println(SingletonDemo.getInstance());
//        System.out.println(SingletonDemo.getInstance());
//        System.out.println(SingletonDemo2.getInstance());
//        System.out.println(SingletonDemo2.getInstance());
//        System.out.println(SingletonDemo2.getInstance());

        System.out.println(SingletonDemo3.getInstance());
        System.out.println(SingletonDemo3.getInstance());
        System.out.println(SingletonDemo3.getInstance());

    }

    @Test
    public void test01()throws Exception{
        //方式一：根据类的clazz获取
        Bean1 bean = SpringBeanUtil.getBean(Bean1.class);
        System.out.println(bean.getBean1());
        Bean2 bean2 = SpringBeanUtil.getBean(Bean2.class);
        System.out.println(bean2.getBean2());

        //方式二：根据bean名字获取
        Bean1 bean11 = (Bean1) SpringBeanUtil.getBean("bean1");
        System.out.println(bean11.getBean1());
        //报错：NoSuchBeanDefinitionException
        Bean2 bean22 = (Bean2) SpringBeanUtil.getBean("bean2");
        //正确
        Bean2 bean222 = (Bean2) SpringBeanUtil.getBean("bean2New");
        System.out.println(bean222.getBean2());
    }

    @Test
    public void test02()throws Exception{
        //根据BeanName获取具体的bean，实现策略模式
        //根据人员ID（或者类型）获取不同的bean
        String beanName = PeopleEnum.getBeanName(2);
        ISpringBeanStrategy bean = (ISpringBeanStrategy) SpringBeanUtil.getBean(beanName);

        String eating = bean.eating();
        System.out.println(eating);

        String play = bean.play();
        System.out.println(play);
    }

    @Resource
    private StrategyByFactory strategyByFactory;

    @Test
    public void test03()throws Exception{
        //使用简单工厂生产的策略模式 —— 明显发现使用起来更简单，把创建bean的权利交给了简单工厂
        IFactoryStrategy businessMap = strategyByFactory.getBusinessMap(22);

        System.out.println(businessMap.eating());
        System.out.println(businessMap.play());

    }
}
