package com.cc.urlgethtml.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/11/7
 */
@Data
@Component
@ConfigurationProperties(prefix = "phantomjs2")
public class PhantomConPro {

    private String binPath2;

    private String binPath3;

}
