package com.cc.urlgethtml.utils;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/11/7
 */
@Component
@ConfigurationProperties(prefix = "phantomjs")
public class PhantomConProMapStatic {

    @Getter
    public static Map<String, String> binPath;
    @Getter
    public static Map<String, String> jsPath;
    @Getter
    public static Map<String, String> imagePath;

    //必须是非静态的set方法
    public void setBinPath(Map<String, String> binPath) {
        PhantomConProMapStatic.binPath = binPath;
    }

    public void setJsPath(Map<String, String> jsPath) {
        PhantomConProMapStatic.jsPath = jsPath;
    }

    public void setImagePath(Map<String, String> imagePath) {
        PhantomConProMapStatic.imagePath = imagePath;
    }

}
