package com.cc.urlgethtml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlGetHtmlApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrlGetHtmlApplication.class, args);
    }

}
