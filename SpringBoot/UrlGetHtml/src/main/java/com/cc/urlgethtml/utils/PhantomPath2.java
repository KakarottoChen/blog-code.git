package com.cc.urlgethtml.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/11/6
 */
@Data
@Component
//@ConfigurationProperties(prefix = "phantomjs")
public class PhantomPath2 {

    private Map<String, String> binPath;
//    private String jsPathWin;
//    private String binPathLinux;
//    private String jsPathLinux;
//    private String imagePathWin;
//    private String imagePathLinux;


}
