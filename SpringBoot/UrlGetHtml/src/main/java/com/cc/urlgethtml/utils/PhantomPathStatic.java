package com.cc.urlgethtml.utils;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>根据不同系统获取不同路径</p>
 *
 * @author CC
 * @since 2023/11/3
 */
@Component
public class PhantomPathStatic {

    public static String binPathWin;
    public static String jsPathWin;

    //必须是非静态的set方法
    @Value("${phantomjs.binPath.windows}")
    public void setBinPathWin(String binPathWin) {
        PhantomPathStatic.binPathWin = binPathWin;
    }
    @Value("${phantomjs.jsPath.windows}")
    public void setJsPathWin( String jsPathWin) {
        PhantomPathStatic.jsPathWin = jsPathWin;
    }

    public static String getBinPathWin() {
        return binPathWin;
    }

    public static String getJsPathWin() {
        return jsPathWin;
    }
}
