package com.cc.urlgethtml.utils;

import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/11/6
 */
@Data
public class JsoupUtils {


    public static String getHtmlByJsoup(String url) {
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return doc.html();
    }


}
