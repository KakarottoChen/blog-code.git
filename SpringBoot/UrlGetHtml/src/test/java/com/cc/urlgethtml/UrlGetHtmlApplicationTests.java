package com.cc.urlgethtml;

import com.cc.urlgethtml.utils.*;
import com.cc.urlgethtml.web.controller.GetBaidu;
import org.junit.jupiter.api.Test;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

@SpringBootTest
class UrlGetHtmlApplicationTests {

    @Resource
    private GetBaidu getBaidu;
    @Resource
    private PhantomTools phantomTools;

    @Test
    void contextLoads() throws FileNotFoundException {
//        String[] cmd = phantomTools.getCmd("http://www.baidu.com", "bd11.png");
//        System.out.println(Arrays.toString(cmd));

        try {
            phantomTools.printUrlScreen2jpg("http://www.baidu.com", "bd11.png");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Resource
    private PhantomPath2 phantomPath2;

    @Test
    public void test02()throws Exception{
        Map<String, String> binPathWin = phantomPath2.getBinPath();
        System.out.println(binPathWin);
    }

    @Resource
    private PhantomPath phantomPath;

    @Test
    public void test03()throws Exception{
        System.out.println(phantomPath.getBinPathWin());
        System.out.println(phantomPath.getJsPathWin());
        System.out.println(phantomPath.getBinPathLinux());
        System.out.println(phantomPath.getJsPathLinux());
        System.out.println(phantomPath.getImagePathWin());
        System.out.println(phantomPath.getImagePathLinux());
    }

    @Resource
    private PhantomPathStatic phantomPathStatic;

    @Test
    public void test04()throws Exception{
        System.out.println(phantomPathStatic.getBinPathWin());
        System.out.println(PhantomPathStatic.binPathWin);
        System.out.println(phantomPathStatic.getJsPathWin());
        System.out.println(PhantomPathStatic.jsPathWin);
    }

    @Resource
    private PhantomConPro phantomConPro;

    @Test
    public void test05()throws Exception{
        System.out.println(phantomConPro.getBinPath2());
        System.out.println(phantomConPro.getBinPath3());
    }

    @Resource
    private PhantomConProMap phantomConProMap;

    @Test
    public void test06()throws Exception{
        System.out.println(phantomConProMap.getBinPath());
        System.out.println(phantomConProMap.getJsPath());
        System.out.println(phantomConProMap.getImagePath());
        //获取map中的值
        System.out.println(phantomConProMap.getBinPath().get("windows"));
    }

    @Resource
    private PhantomConProMapStatic phantomConProMapStatic;

    @Test
    public void test07()throws Exception{
        System.out.println(PhantomConProMapStatic.binPath);
        System.out.println(PhantomConProMapStatic.getBinPath());
        //获取map中的值
        System.out.println(phantomConProMapStatic.getBinPath().get("windows"));

        System.out.println(PhantomConProMapStatic.jsPath);
    }

}
