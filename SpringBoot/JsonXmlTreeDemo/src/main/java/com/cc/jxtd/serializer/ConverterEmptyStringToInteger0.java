package com.cc.jxtd.serializer;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.util.StdConverter;

/** 序列化：1将string转为int。
 *         2转换String的空或null -》 转为Integer的0
 * @author --
 * @since 2024/4/18
 **/
public class ConverterEmptyStringToInteger0 extends StdConverter<String, Integer> {

    @Override
    public Integer convert(String value) {
        //把空的string转为int的0
        if (value == null || value.trim().isEmpty()) {
            return 0;
        }
        return Integer.valueOf(value);
    }

}