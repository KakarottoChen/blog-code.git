package com.cc.jxtd.aspect;

import com.cc.jxtd.annotation.EmptyToNull;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Objects;

/** 切面
 * @author --
 */
@Aspect
@Component
public class UpdateAspect {

    private static final Logger logger = LoggerFactory.getLogger(UpdateAspect.class);

    //切入点
    @Pointcut("@annotation(com.cc.jxtd.annotation.OptConverter)")
    public void validPointCut() {
    }

    /**
     * 环绕修改参数
     */
    @Around("validPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object[] args = point.getArgs();
        Object arg = args[0];
        //2、切面+反射：全部转换
//        this.allEmptyToNull(arg);
        //3、注解+切面+反射：部分转换
        this.assignEmptyToNull(arg);

        return point.proceed();
    }

    /**
     * 设置请求参数中 所有字段的空值（如：String的空字符串）为null
     * @param arg arg
     */
    public void allEmptyToNull(Object arg) {
        if (Objects.isNull(arg)) {
            return;
        }
        Field[] fields = arg.getClass().getDeclaredFields();
        for (Field field : fields) {
            // 设置字段可访问
            field.setAccessible(true);
            // 如果字段是 String 类型且值为空字符串，则设置为 null
            if (field.getType() == String.class) {
                try {
                    String value = (String) field.get(arg);
                    if (value != null && value.isEmpty()) {
                        field.set(arg, null);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            // 可以扩展其他类型的参数……

        }
    }

    /** 指定空转null
     * @param arg arg
     * @since 2024/4/18
     **/
    private void assignEmptyToNull(Object arg) {
        if (Objects.isNull(arg)) {
            return;
        }
        Field[] fields = arg.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(EmptyToNull.class)) {
                // 设置字段可访问
                field.setAccessible(true);
                // 如果字段是 String 类型且值为空字符串，则设置为 null
                if (field.getType() == String.class) {
                    try {
                        String value = (String) field.get(arg);
                        if (value != null && value.isEmpty()) {
                            field.set(arg, null);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                // 可以扩展其他类型的参数……

            }

        }
    }


}
