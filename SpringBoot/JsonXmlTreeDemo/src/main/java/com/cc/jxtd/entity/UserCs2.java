package com.cc.jxtd.entity;

import com.cc.jxtd.annotation.EmptyToNull;
import lombok.Data;

/**
 * <p>请求，返回都用这个</p>
 *
 * @author --
 * @since 2024/4/19
 */
@Data
public class UserCs2 {

    private Long id;

    private String name;

    @EmptyToNull
    private String desc;

}
