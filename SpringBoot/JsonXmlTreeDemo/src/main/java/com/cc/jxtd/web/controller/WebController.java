package com.cc.jxtd.web.controller;

import com.cc.jxtd.util.BuildTree;
import com.cc.jxtd.util.CsTreeNode;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author --
 * @since 2024/4/12
 */
@RestController
@RequestMapping("/web")
public class WebController {

    @Resource
    private BuildTree buildTree;

    @PostMapping("/json")
    public List<CsTreeNode> get(@RequestBody String jsonStr){
        List<CsTreeNode> csTreeNodes = buildTree.buildTree(jsonStr);
        return csTreeNodes;
    }

    @PostMapping("/jsonValue")
    public Object getJsonOrXml(@RequestBody Map<String, String> map){
        String jsonStr = map.get("jsonStr");
        String uniquePath = map.get("uniquePath");

        Object jsonOrXml = buildTree.getJsonOrXml(jsonStr,
                Lists.newArrayList(), "", uniquePath, false);
        return jsonOrXml;
    }



}

