package com.cc.jxtd.entity;

import com.cc.jxtd.serializer.ConverterEmptyStringToNull;
import com.cc.jxtd.serializer.EmptyStringToNullDeserializer;
import com.cc.jxtd.serializer.ConverterEmptyStringToInteger0;
import com.cc.jxtd.serializer.EmptyStringToNullSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.naming.Name;

/**
 * <p>请求，返回都用这个</p>
 *
 * @author --
 * @since 2024/4/19
 */
@Data
public class UserCs {


    private Long id;

    //反序列化（前端请求）：空字符串为null
    @JsonDeserialize(using = EmptyStringToNullDeserializer.class)
    private String name;

    //反序列化（前端请求）：转换：为其他类型的值（转换为int的0）
    @JsonDeserialize(converter = ConverterEmptyStringToInteger0.class)
    private Integer descConverter0;

    //序列化（后端返回）：空字符串为null
    @JsonSerialize(using = EmptyStringToNullSerializer.class)
    private String descSerialize;

    //序列化（后端返回）：转换：空字符串转为null
    @JsonSerialize(converter = ConverterEmptyStringToNull.class)
    private String descConverterNull;

}
