package com.cc.jxtd.util;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author --
 * @since 2024/1/11
 */
@Data
public class CsTreeNode {

    /**
     * 当前节点向上的所有节点名字
     */
    private String id;

    /**
     * 父节点id
     */
    private String pid;

    /**
     * 节点名字
     */
    private String nodeName;

    /**
     * 1对象；2数组；3最下级的对象
     */
    private String type = "1";

    /**
     * 具体的值。只有type=3时才有
     */
    private Object mapValue;

    /**
     * 路径
     */
    private String path;

    /**
     * 唯一路径
     */
    private String uniquePath;

    /**
     * 子级
     */
    private List<CsTreeNode> children = Lists.newArrayList();

}
