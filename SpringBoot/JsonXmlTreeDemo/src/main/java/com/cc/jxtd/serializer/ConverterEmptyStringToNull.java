package com.cc.jxtd.serializer;

import com.fasterxml.jackson.databind.util.StdConverter;

/** 序列化-转换：将string的空转为null
 * @author --
 * @since 2024/4/18
 **/
public class ConverterEmptyStringToNull extends StdConverter<String, String> {

    @Override
    public String convert(String value) {
        //把空的string转为int的0
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        return value;
    }

}