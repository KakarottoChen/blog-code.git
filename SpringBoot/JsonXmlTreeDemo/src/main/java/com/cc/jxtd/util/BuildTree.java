package com.cc.jxtd.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.google.common.collect.Lists;
import com.jayway.jsonpath.JsonPath;
import com.sun.istack.internal.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p></p>
 *
 * @author --
 * @since 2024/4/12
 */
@Component
public class BuildTree {

    public static final String ONE_STR = "1";
    public static final String TWO_STR = "2";
    public static final String THREE_STR = "3";
    public static final String TYPE = "type";
    public static final String VAL = "val";

    /**
     * @param jsonStr "JSON字符串"
     * @return List<CsTreeNode>
     */
    public List<CsTreeNode> buildTree(String jsonStr) {

        //下载文件数据
        Object parseJson;
        try {
            //json可以从远程下载、也可以传入json字符串
            //String fileData = HttpUtil.downloadString(curFile.getFilePath(), CharsetNames.UTF_8);
            //Feature.OrderedField：有序输出JSON，不然json是乱序的
            parseJson = JSON.parse(jsonStr, Feature.OrderedField);
        }catch(Exception e){
            throw new RuntimeException(String.format("下载文件数据或数据不是json格式的，请检查文件！错误：%s", e.getMessage()));
        }
        if (Objects.isNull(parseJson)) {
            return Lists.newArrayList();
        }

        //递归构建json树，pPath：$：跟节点的path符号。
        return build(parseJson, null, "$");
    }

    private static List<CsTreeNode> build(Object parseJson, String pid, String pPath) {
        List<CsTreeNode> tree = Lists.newArrayList();
        if (parseJson instanceof JSONObject) {
            JSONObject object = (JSONObject) parseJson;
            Set<String> keySet = object.keySet();
            keySet.forEach(key -> {
                Object object1 = object.get(key);
                CsTreeNode node = new CsTreeNode();

                //构建jsonPath的通用path（FastJson也遵守）
                String path = pPath.concat(String.format("['%s']", key));
                node.setPath(path);
                node.setUniquePath(path);

                node.setId(getKey(pid, key));
                node.setNodeName(key);
                node.setPid(pid);

                //构建子级
                if (object1 instanceof JSONObject) {
                    node.setType(ONE_STR);
                }else if (object1 instanceof JSONArray) {
                    JSONArray jas = (JSONArray)object1;
                    //如果是数组，查询下级：下级如果不是json了，就是值，需要存入该节点的值中
                    List<JSONObject> values = Lists.newArrayList();
                    jas.forEach(child -> {
                        if (!(child instanceof JSONObject) && !(child instanceof JSONArray)) {
                            Class<?> aClass = getaClass(child);
                            JSONObject msg = new JSONObject();
                            msg.put(TYPE, aClass);
                            msg.put(VAL, child);
                            values.add(msg);
                        }
                    });
                    node.setMapValue(values);

                    node.setType(TWO_STR);
                }else {
                    //下面是值了。插入值。
                    node.setType(THREE_STR);
                    JSONObject msg = new JSONObject();
                    Class<?> aClass = getaClass(object1);
                    msg.put(TYPE, aClass);
                    msg.put(VAL, object1);
                    node.setMapValue(msg);
                }

                node.setChildren(build(object1, node.getId(), node.getUniquePath()));
                tree.add(node);
            });
        }else if (parseJson instanceof JSONArray) {
            //数组类型的，每一个对象名字：使用对应的下标
            JSONArray array = (JSONArray) parseJson;
            for (int i = 0; i < array.size(); i++) {
                Object child = array.get(i);
                //子级是json才继续构建子级，如果不是json是具体的值了，直接舍弃。
                if (child instanceof JSONObject || child instanceof JSONArray) {
                    CsTreeNode node = new CsTreeNode();
                    String arrName = String.format("[%d]", i);

                    //构建jsonPath的通用path（FastJson也遵守）
                    String path = pPath.concat(arrName);
                    node.setPath(path);
                    node.setUniquePath(path);

                    node.setId(getKey(pid, arrName));
                    node.setNodeName(arrName);
                    node.setPid(pid);
                    node.setType(TWO_STR);

                    node.setChildren(build(child, node.getId(), node.getUniquePath()));
                    tree.add(node);
                }
            }
        }
        return tree;
    }

    @NotNull
    private static Class<?> getaClass(Object object1) {
        Class<?> aClass;
        if (object1 instanceof String) {
            aClass = String.class;
        }else if (object1 instanceof Integer) {
            aClass = Integer.class;
        }else if (object1 instanceof Long) {
            aClass = Long.class;
        }else if (object1 instanceof BigDecimal) {
            aClass = BigDecimal.class;
        }else if (object1 instanceof Date) {
            //todo 时间类型需要单独获取。
            aClass = Date.class;
        }else {
            aClass = Object.class;
        }
        return aClass;
    }

    @NotNull
    private static String getKey(String pid, String key) {
        pid = StringUtils.isBlank(pid) ? "" : pid;
        String concat = pid.concat(":").concat(key);
        return StrUtil.removePrefix(concat, ":");
    }


    /**
     * 解析结构化文件，并获取json、xml的片段
     *
     * @param jsonStr json字符串
     * @param nodeIds 解析后的节点ids
     * @param path 节点唯一路径
     * @param uniquePath 节点唯一路径
     * @param isCompress 是否压缩：true：是压缩；false：不压缩
     * @return {@link Object}
     * @since 2024/1/22
     **/
    public Object getJsonOrXml(String jsonStr, List<String> nodeIds,
                               String path, String uniquePath, boolean isCompress) {

        //下载文件数据
        Object parseJson;
        try {
//            String fileData = HttpUtil.downloadString(semi.getFilePath(), CharsetNames.UTF_8);
            //Feature.OrderedField：有序输出JSON，不然json是乱序的
            parseJson = JSON.parse(jsonStr, Feature.OrderedField);
        }catch(Exception e){
            throw new RuntimeException(String.format("下载文件数据或数据不是json格式的，请检查文件！错误：%s", e.getMessage()));
        }
        if (Objects.isNull(parseJson)) {
            return new JSONObject();
        }

        Boolean isPd = Boolean.TRUE;

        //1构建：选择的节点下的json模式——JsonPath
        if (isPd) {
            //阿里巴巴的fastjson的JSONPath无法解析：key是：[2]的值。
//            Object read1 = JSONPath.read(parseJson.toString(), "$[1].'[2]'");
//            String string1 = read1.toString();
//            Object read2 = JSONPath.read(parseJson.toString(), uniquePath);
            //jsonPath构建方式，可以解析：key是：[2]的值。
            return JsonPath.read(parseJson, uniquePath);
        }

        //2构建：全路径模式——自己通过id写的。
        if (!isPd) {
            //第一次循环：获取最后一级的json
            List<JSON> jsons = Lists.newArrayList();
            for (int i = 0; i < nodeIds.size(); i++) {
                String nodeId = nodeIds.get(i);
                if (parseJson instanceof JSONObject) {
                    JSONObject jo = (JSONObject) parseJson;
                    parseJson = jo.get(nodeId);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(nodeId, null);
                    jsons.add(jsonObject);
                }else if (parseJson instanceof JSONArray) {
                    JSONArray jo = (JSONArray) parseJson;

                    String ind = StrUtil.subBetween(nodeId, "[", "]");
                    //改变需要构建的值
                    parseJson = jo.get(Integer.parseInt(ind));

                    jsons.add(new JSONArray());
                }
            }

            //第二次循环：倒着循环，构建JSON树
            Collections.reverse(jsons);
            JSON child = null;
            for (int i = 0; i < jsons.size(); i++) {
                JSON json = jsons.get(i);
                if (json instanceof JSONObject) {
                    JSONObject aa = ((JSONObject) json);
                    Set<String> keySet = aa.keySet();
                    String k = null;
                    for (String key : keySet) {
                        k = key;
                    }
                    //第一次，子级为最下级的json
                    if (i == 0) {
                        aa.put(k, parseJson);
                    }else {
                        //后面添加child为子级
                        aa.put(k, child);
                    }
                    child = aa;
                }
                if (json instanceof JSONArray) {
                    JSONArray aa = ((JSONArray) json);
                    if (i == 0) {
                        aa.add(parseJson);
                    }else {
                        aa.add(child);
                    }
                    child = aa;
                }
            }

            return child;
        }

        return null;
    }


}
