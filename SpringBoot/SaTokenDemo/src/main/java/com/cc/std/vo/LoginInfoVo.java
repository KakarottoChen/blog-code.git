package com.cc.std.vo;

import com.cc.std.entity.StUser;
import lombok.Data;

import java.util.Map;

/** 登录后返回的参数
 * @author CC
 * @since 2023/3/6 0006
 */
@Data
public class LoginInfoVo extends StUser {

    /**
     * token的名字
     */
    String tokenName;

    /**
     * token的值
     */
    String tokenValue;

    /**
     * 整个token
     */
    Map<String,String> token;

}
