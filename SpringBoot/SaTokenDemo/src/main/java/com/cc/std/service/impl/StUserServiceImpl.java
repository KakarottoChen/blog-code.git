package com.cc.std.service.impl;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cc.std.entity.StUser;
import com.cc.std.mapper.StUserMapper;
import com.cc.std.req.LoginReq;
import com.cc.std.service.IStUserService;
import com.cc.std.vo.LoginInfoVo;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author CC
 * @since 2023/3/6 0006
 */
@Service
public class StUserServiceImpl extends ServiceImpl<StUserMapper, StUser> implements IStUserService {

    /** 登陆的方法，可以根据不同业务增加各自的逻辑
     * @param req 请求参数
     * @return com.cc.std.vo.LoginInfoVo
     * @since 2023/3/7 0007
     * @author CC
     **/
    @Override
    public LoginInfoVo login(LoginReq req) {
        LambdaQueryWrapper<StUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StUser::getUsername,req.getUserName());
        wrapper.eq(StUser::getPassword,req.getPassword());
        StUser user = super.getOne(wrapper);

        LoginInfoVo vo = new LoginInfoVo();
        if (Objects.isNull(user)){
            return vo;
        }

        //登陆——使用用户id。也可以用需要返回给前端的信息进行登陆
        StpUtil.login(user.getId());

        String tokenName = StpUtil.getTokenName();
        String tokenValue = StpUtil.getTokenValue();
        vo.setTokenName(tokenName);
        vo.setTokenValue(tokenValue);

        BeanUtil.copyProperties(user,vo);
        String jsonStr = JSONUtil.toJsonStr(vo);

        //将登陆信息保存到session方式一：将登陆后需要保存的信息存在 Token-Session（多端都是唯一）
//        SaSession tokenSession = StpUtil.getTokenSession();
        //key：固定的值
//        tokenSession.set("ss",jsonStr);

        //将登陆信息保存到session方式二：将登陆后需要保存的信息存在 User-Session（多端可共享）
        SaSession userSession = StpUtil.getSession();
        //key：必须使用不同的值（取值需要对应，才能取出对应的session）
        //value：直接存值，不需要存json字符串。取值时，使用model可以直接取出来。
        userSession.set(user.getId(),vo);

        return vo;
    }
}
