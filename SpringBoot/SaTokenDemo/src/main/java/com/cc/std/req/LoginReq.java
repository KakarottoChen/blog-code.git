package com.cc.std.req;

import lombok.Data;

/**
 * @author CC
 * @since 2023/3/6 0006
 */
@Data
public class LoginReq {

    /**
     * 登陆用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

}
