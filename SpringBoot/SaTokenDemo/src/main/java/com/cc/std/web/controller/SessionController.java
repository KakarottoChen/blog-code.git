package com.cc.std.web.controller;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONString;
import cn.hutool.json.JSONUtil;
import com.cc.std.utils.LoginInfoUtil;
import com.cc.std.vo.LoginInfoVo;
import com.google.common.collect.Maps;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author CC
 * @since 2023/3/7 0007
 */
@RequestMapping("/session")
@RestController
public class SessionController {

    @GetMapping
    public String getSession(){
        //获取token的值
        String tokenValue = StpUtil.getTokenValue();
        //获取（登陆的用户的id）方式1
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        Object loginId = tokenInfo.getLoginId();
        //获取（登陆的用户的id）方式2
        Object loginId1 = StpUtil.getLoginId();

        //获取 tokenSession 保存的值
        SaSession tokenSession = StpUtil.getTokenSession();
        Object ss = tokenSession.get("ss");

        //获取 userSession 保存的值
        SaSession userSession = StpUtil.getSession();
        Object userSessionStr = userSession.get(loginId.toString());

        return JSONUtil.toJsonStr(userSessionStr);
    }

    /** 使用登陆工具类获取当前登陆用户的信息（这个demo最终需要达到的需求）
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @since 2023/3/7 0007
     * @author CC
     **/
    @GetMapping("/all")
    public Map<String,Object> getSessionAll(){
        String userId = LoginInfoUtil.getUserId();
        String name = LoginInfoUtil.getName();
        String username = LoginInfoUtil.getUsername();
        String password = LoginInfoUtil.getPassword();
        Map<String, String> token = LoginInfoUtil.getToken();

        Map<String,Object> map = Maps.newHashMap();
        map.put("userId",userId);
        map.put("name",name);
        map.put("username",username);
        map.put("password",password);
        map.put("token",token);

        return map;
    }

}
