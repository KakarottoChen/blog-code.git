package com.cc.std.web.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.cc.std.entity.StUser;
import com.cc.std.req.LoginReq;
import com.cc.std.service.IStUserService;
import com.cc.std.vo.LoginInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CC
 * @since 2023/3/6 0006
 */
@RestController
@RequestMapping("/user")
public class StUserController {

    @Autowired
    private IStUserService userService;

    @GetMapping
    public List<StUser> getUsers (){
        return userService.list();
    }

    /** 登陆
     * @param req
     * @return
     */
    @PostMapping("/login")
    public LoginInfoVo login(@RequestBody LoginReq req){
        return userService.login(req);
    }

    @GetMapping("/logout")
    public String logout(){
        // 当前会话注销登录
        StpUtil.logout();
        return "操作成功！";
    }



}
