# SpringBoot项目预加载数据
- 参考：
 https://www.cnblogs.com/java-chen-hao/p/11835120.html#_label1
 https://zhuanlan.zhihu.com/p/541268993
- 业务需求：
缓存数据字典数据、初始化线程池、提前加载好加密证书

## 1、方式
- 实现 ApplicationRunner 接口
- 实现 CommandLineRunner 接口
- 实现 InitializingBean 接口
- 使用 @PostConstruct 标签

## 2、@Order
- 可以使用@Order注解或Ordered接口改变 ApplicationRunner 和 CommandLineRunner执行顺序
- @Order 对 InitializingBean 和 @PostConstruct 不生效。

## 3、测试使用
- ApplicationRunner
```java
@Component
@Order(3)
public class App01 implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("App01_执行了……@Order(3)");
    }

}
```
---
```java
@Component
@Order(2)
public class App02 implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("App02_执行了……@Order(2)");
    }
    
}
```
- CommandLineRunner
```java
@Component
@Order(1)
public class Com01 implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Com01_执行了……@Order(1)");
    }

}
```
---
```java
@Component
@Order(0)
public class Com02 implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Com02_执行了……@Order(0)");
    }

}
```

- InitializingBean
```java
/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(7)
public class Ini01 implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Ini01_执行了……@Order(7)");
    }

}
```
---
```java
/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(4)
public class Ini02 implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Ini02_执行了……@Order(4)");
    }

}
```

- @PostConstruct
```java
/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(6)
public class Pos01 {

    @PostConstruct
    public void customizeName(){
        System.out.println("Pos01_执行了……@Order(6)");
    }
    
}
```
---
```java
/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(5)
public class Pos02 {

    @PostConstruct
    public void customizeName(){
        System.out.println("Pos02_执行了……@Order(5)");
    }
    
}
```

## 4、执行顺序、建议使用
- 通过实现得出执行顺序
```properties
InitializingBean > @PostConstruct > ApplicationRunner > CommandLineRunner
```

- ==没有执行顺序要求==，使用：==@PostConstruct==
- ==有执行顺序要求==，使用：==ApplicationRunner(推荐)==或者CommandLineRunner