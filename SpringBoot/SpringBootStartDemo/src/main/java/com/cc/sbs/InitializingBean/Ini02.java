package com.cc.sbs.InitializingBean;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(4)
public class Ini02 implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Ini02_执行了……@Order(4)");
    }

}
