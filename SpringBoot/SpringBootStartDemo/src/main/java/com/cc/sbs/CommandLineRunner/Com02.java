package com.cc.sbs.CommandLineRunner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(0)
public class Com02 implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Com02_执行了……@Order(0)");
    }

}
