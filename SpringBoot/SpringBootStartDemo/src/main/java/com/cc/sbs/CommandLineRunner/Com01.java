package com.cc.sbs.CommandLineRunner;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(1)
public class Com01 implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Com01_执行了……@Order(1)");
    }

}


