package com.cc.sbs.PostConstruct;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(5)
public class Pos02 {

    @PostConstruct
    public void customizeName(){
        System.out.println("Pos02_执行了……@Order(5)");
    }

}
