package com.cc.sp.druid.utils;

import com.alibaba.druid.DbType;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.cc.sp.druid.visit.MyOracleVisit;

/** 德鲁伊解析sql的工具类
 * @author CC
 * @since 2023/5/4 0004
 */
public class DruidSqlParseUtil {

    /**
     * <p>获取sql的别名（暂时支持dm、oracle）</p>
     * <ul>
     *     <li>如：SELECT * FROM table1 AS ttttt1</li>
     *          获取的是：ttttt1
     *     <li>如：SELECT * FROM table1 AS ttttt2</li>
     *          获取的是：ttttt2
     *     <li>如：SELECT * FROM (SELECT * FROM TD_WD_TEMP_REPORT AS TR
     *            LEFT JOIN TD_WD_STRUCTURE AS STR ON STR.TEMP_REPORT_ID = TR.ID )
     *            AS TTTTTT3
     *     </li>
     *          获取的是：TTTTTT3
     * </ul>
     *
     * @param sql sql
     * @return java.lang.String
     * @author CC
     * @since 2023/4/19 0019
     **/
    public static String getAlias(String sql) {
//        if (StringUtils.isBlank(sql)) {
//            throw new QzBizException("sql不能为空！");
//        }
        String alias = "";
        //一、达梦、oracle
        SQLStatement sqlStatement = SQLUtils.parseSingleStatement(sql, DbType.oracle, true);
        if (sqlStatement instanceof SQLSelectStatement) {
            SQLSelectStatement selectStatement = (SQLSelectStatement) sqlStatement;
            //1直接获取方式
//            SQLSelect select = selectStatement.getSelect();
//            SQLSelectQueryBlock query = (SQLSelectQueryBlock) select.getQuery();
//            SQLTableSource from = query.getFrom();
//            String alias1 = from.getAlias();

            //2使用Visit获取方式
            MyOracleVisit dmVisit = new MyOracleVisit();
            selectStatement.accept(dmVisit);
            alias = dmVisit.getAlias();
        }
        //其他数据库...待完善...

        return alias;
    }




}
