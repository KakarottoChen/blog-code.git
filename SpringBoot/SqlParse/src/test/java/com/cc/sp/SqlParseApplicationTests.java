package com.cc.sp;

import com.cc.sp.druid.utils.DruidSqlParseUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SqlParseApplicationTests {

    @Test
    void contextLoads() {
        String sql = "select * from T1 AS TTTT1";
        String alias = DruidSqlParseUtil.getAlias(sql);
        System.out.println(alias);
//        System.out.println(1);
    }

}
