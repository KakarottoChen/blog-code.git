package com.cc.jsl.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCs {

    private String name;

    private Integer age;


}
