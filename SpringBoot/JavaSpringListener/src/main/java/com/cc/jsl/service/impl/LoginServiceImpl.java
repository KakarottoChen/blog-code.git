package com.cc.jsl.service.impl;

import com.cc.jsl.event.EmailEvent;
import com.cc.jsl.service.ILoginService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>事件发布者（Event Publisher）</p>
 *
 * @author CC
 * @since 2023/10/10
 */
@Service
public class LoginServiceImpl implements ILoginService {

    @Resource
    private ApplicationEventPublisher eventPublisher;

    @Override
    public void login(){
        //登陆逻辑...

        //事件发布
        // 1、可以直接发送JavaBean的参数：如String、自定义类
//        eventPublisher.publishEvent("字符串参数！");
//        eventPublisher.publishEvent(new UserCs("cs" , 18));
//        eventPublisher.publishEvent(34);

        // 2、发送事件参数（继承了ApplicationEvent的类）
        EmailEvent emailEvent = new EmailEvent(this, "cc", 18);
        eventPublisher.publishEvent(emailEvent);

    }

}
