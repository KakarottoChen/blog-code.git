package com.cc.jsl;

import com.cc.jsl.service.ILoginService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class ApplicationTests {

    @Resource
    private ILoginService loginService;

    @Test
    void contextLoads() {

        loginService.login();

    }

}