package com.cc.queuedemo.BlockingQueue;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author CC
 * @since 2023/3/13 0013
 */
@Component
public class ArrayBlockingQueueDemo {

    public final static ArrayBlockingQueue<String> ABQ = new ArrayBlockingQueue<>(5);


    public static boolean addAbq(String str){
        try {
            System.out.println("放进队列（前）：" + str);
            ABQ.put(str);
            System.out.println("放进队列（后）：" + str);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    /** 启动就开始取队列
     **/
    @PostConstruct
    public static void taskAbq(){
        System.out.println("开始取…");
        while (true){
            try {
                String take = ABQ.take();
                System.out.println("取出的队列：" + take);
//                return take;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                boolean addAbq = addAbq(String.valueOf(i));
            }
        });
        thread.run();

//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
//        Thread thread1 = new Thread(() -> {
//            taskAbq();
//        });
//        thread1.run();
//        taskAbq();

    }

}
