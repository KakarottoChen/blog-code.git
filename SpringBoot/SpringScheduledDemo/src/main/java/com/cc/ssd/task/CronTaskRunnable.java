package com.cc.ssd.task;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/** 具体任务实现
 * @author CC
 * @since 2023/4/21 0021
 */
@Data
public class CronTaskRunnable implements Runnable{

    private static final Logger log = LoggerFactory.getLogger(CronTaskRunnable.class);

    /**
     * 任务id（必须唯一）
     */
    private String taskId;
    /**
     * 任务类型：自定义
     */
    private Integer taskType;
    /**
     * 任务名字
     */
    private String taskName;
    /**
     * 任务参数
     */
    private Object[] params;

    public CronTaskRunnable() {
    }

    public CronTaskRunnable(String taskId, Integer taskType, String taskName, Object... params) {
        this.taskId = taskId;
        this.taskType = taskType;
        this.taskName = taskName;
        this.params = params;
    }

    /** 执行任务
     * @since 2023/4/21 0021
     * @author CC
     **/
    @Override
    public void run() {
        long start = System.currentTimeMillis();

        //具体的任务。
        log.info("\n\t {}号.定时任务开始执行 - taskId：{}，taskName：{}，taskType：{}，params：{}",
                taskType,taskId,taskName,taskType,params);

        //任务处理的方式：
        //todo cc 1就在这里执行：模拟任务
        //todo cc 2开启策略模式，根据任务类型 调度不同的任务
        //todo cc 3使用反射：传来bean名字，方法名字，调用不同的任务
        //todo cc 4开启队列，把要执行的任务放到队列中，然后执行 —— 使用场景：每个任务执行很耗时的情况下使用
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        log.info("\n\t {}号.任务执行完成 - 耗时：{}，taskId：{}，taskType：{}",
                taskType, System.currentTimeMillis()- start , taskId, taskType);

    }





}
