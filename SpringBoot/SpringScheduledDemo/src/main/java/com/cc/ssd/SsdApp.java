package com.cc.ssd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cc
 */
@SpringBootApplication
public class SsdApp {

    public static void main(String[] args) {
        SpringApplication.run(SsdApp.class, args);
    }

}
