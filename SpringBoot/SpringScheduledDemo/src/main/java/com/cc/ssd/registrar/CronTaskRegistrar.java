package com.cc.ssd.registrar;

import com.cc.ssd.task.CronTaskFuture;
import com.cc.ssd.task.CronTaskRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/** 注册定时任务：缓存定时任务、注册定时任务到调度中心
 * @author CC
 **/
@Component
public class CronTaskRegistrar implements DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(CronTaskRegistrar.class);

    /**
     * 缓存任务
     * key：具体的任务
     * value：注册定时任务后返回的ScheduledFuture
     */
    private final Map<Runnable, CronTaskFuture> scheduledTasks = new ConcurrentHashMap<>(16);

    /**
     * 使用自定义的任务调度配置
     */
    @Resource(name = "taskScheduler")
    private TaskScheduler taskScheduler;

    /** 获取任务调度配置
     * @return 任务调度配置
     */
    public TaskScheduler getTaskScheduler() {
        return this.taskScheduler;
    }

    /** 新增定时任务1
     *  存在任务：删除此任务，重新新增这个任务
     * @param taskRunnable 执行的具体任务定义：taskRunnable 实现Runnable
     * @param cronExpression cron表达式
     */
    public void addCronTask(Runnable taskRunnable, String cronExpression) {
        //验证cron表达式是否正确
        boolean validExpression = CronExpression.isValidExpression(cronExpression);
        if (!validExpression){
            throw new RuntimeException("cron表达式验证失败！");
        }
        //获取下次执行时间
        CronExpression parse = CronExpression.parse(cronExpression);
        LocalDateTime next = parse.next(LocalDateTime.now());
        if (Objects.nonNull(next)) {
            //定时任务下次执行的时间
            String format = next.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            log.info("定时任务下次执行的时间：{}",format);
        }

        //封装成 CronTask（cron任务）
        CronTask cronTask = new CronTask(taskRunnable, cronExpression);
        this.addCronTask(cronTask);
    }

    /** 新增定时任务2
     * @param cronTask ：<p>CronTask用于在指定时间间隔内执行定时任务。</p>
     *                   <p>它是通过CronTrigger来实现的，CronTrigger是一个基于cron表达式的触发器，</p>
     *                   <p>可以在指定的时间间隔内触发任务执行。</p>
     * @since 2023/4/21 0021
     * @author CC
     **/
    private void addCronTask(CronTask cronTask) {
        if (Objects.nonNull(cronTask)) {
            //1有这个任务，先删除这个任务。再新增
            Runnable task = cronTask.getRunnable();
            String taskId = null;
            if (task instanceof CronTaskRunnable){
                taskId = ((CronTaskRunnable) task).getTaskId();
            }
            //通过任务id获取缓存的任务，如果包含则删除，然后新增任务
            Runnable taskCache = this.getTaskByTaskId(taskId);
            if (Objects.nonNull(taskCache) && this.scheduledTasks.containsKey(taskCache)) {
                this.removeCronTaskByTaskId(taskId);
            }
            //2注册定时任务到调度中心
            CronTaskFuture scheduledFutureTask = this.scheduleCronTask(cronTask);

            //3缓存定时任务
            this.scheduledTasks.put(task, scheduledFutureTask);

            //todo cc 4可以将任务保存到数据库中……重新启动程序然后加载数据库中的任务到缓存中……

        }
    }

    /** 注册 ScheduledTask 定时任务
     * @param cronTask cronTask
     * @return 注册定时任务后返回的 ScheduledFutureTask
     */
    private CronTaskFuture scheduleCronTask(CronTask cronTask) {
        //注册定时任务后记录的Future
        CronTaskFuture scheduledTask = new CronTaskFuture();
        //开启定时任务的真正方法
        scheduledTask.future = this.taskScheduler.schedule(cronTask.getRunnable(), cronTask.getTrigger());
//        scheduledTask.setThreadLocal(this.taskScheduler.schedule(cronTask.getRunnable(), cronTask.getTrigger()));
        return scheduledTask;
    }

    /** 获取任务列表
     * @return
     */
    public List<CronTaskRunnable> getScheduledTasks(){
        List<CronTaskRunnable> tasks = new ArrayList<>();

        Set<Runnable> keySet = scheduledTasks.keySet();
        keySet.forEach(key -> {
            CronTaskRunnable task = new CronTaskRunnable();
            if (key instanceof CronTaskRunnable){
                CronTaskRunnable taskParent = (CronTaskRunnable) key;
                BeanUtils.copyProperties(taskParent,task);
            }
            tasks.add(task);
        });

        return tasks.stream()
                .sorted(Comparator.comparing(CronTaskRunnable::getTaskId))
                .collect(Collectors.toList());
    }

    /** 根据任务id删除单个定时任务
     * @param taskId 任务id
     */
    public void removeCronTaskByTaskId(String taskId) {
        //通过任务id获取任务
        Runnable task = this.getTaskByTaskId(taskId);
        //需要通过任务id获取任务，然后再移除
        CronTaskFuture cronTaskFuture = this.scheduledTasks.remove(task);
        if (Objects.nonNull(cronTaskFuture)){
            cronTaskFuture.cancel();
        }
    }

    /** 通过任务id获取任务。未查询到返回null
     * @param taskId 任务id
     * @return java.lang.Runnable
     * @since 2023/4/21 0021
     * @author CC
     **/
    private Runnable getTaskByTaskId(String taskId) {
        Assert.notNull(taskId,"任务id不能为空！");
        Set<Map.Entry<Runnable, CronTaskFuture>> entries = scheduledTasks.entrySet();
        //根据任务id获取该任务缓存
        Map.Entry<Runnable, CronTaskFuture> rcf = entries.stream().filter(rf -> {
            Runnable key = rf.getKey();
            String taskId1 = null;
            if (key instanceof CronTaskRunnable) {
                taskId1 = ((CronTaskRunnable) key).getTaskId();
            }
            return taskId.equals(taskId1);
        }).findAny().orElse(null);

        if (Objects.nonNull(rcf)){
            return rcf.getKey();
        }
        return null;
    }

    /** 删除所有的定时任务
     *     DisposableBean是Spring框架中的一个接口，它定义了一个destroy()方法，
     *     用于在Bean销毁时执行清理工作。
     *     当一个Bean实现了DisposableBean接口时，
     *     Spring容器会在该Bean销毁时自动调用destroy()方法，
     *     以便进行一些清理工作，例如释放资源等。
     *     如果您的Bean需要在销毁时执行一些清理工作，
     *     那么实现DisposableBean接口是一个很好的选择。
     */
    @Override
    public void destroy() {
        //关闭所有定时任务
        for (CronTaskFuture task : this.scheduledTasks.values()) {
            task.cancel();
        }
        //清空缓存
        this.scheduledTasks.clear();

        log.info("取消所有定时任务！");
        //todo cc 修改或删除数据库的任务
    }

}
