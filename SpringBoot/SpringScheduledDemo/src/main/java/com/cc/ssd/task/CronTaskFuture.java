package com.cc.ssd.task;

import java.util.Objects;
import java.util.concurrent.ScheduledFuture;

/** CronTaskFuture类中使用的是ScheduledFuture对象来表示定时任务的执行结果。
 *  ——最后ps：也可以不要这个记录类，直接缓存ScheduledFuture对象。
 *  用来记录单独的Future、定时任务注册任务后产生的
 * @author CC
 **/
public final class CronTaskFuture {

    /** 每个线程一个副本
     * 经过测试这里不能使用ThreadLocal
     */
//    private static final ThreadLocal<ScheduledFuture<?>> THREAD_LOCAL = new ThreadLocal<>();

    /** 最后ps：由于ScheduledFuture是线程安全的。这里不用 volatile 或者 ThreadLocal
     *      注册任务后返回的：ScheduledFuture 用于记录并取消任务
     *      这两个都可以不使用。直接给future赋值
     *          volatile：线程之间可见：volatile用于实现多线程之间的可见性和一致性，保证数据的正确性。
     *          ThreadLocal：用于实现线程封闭，保证线程安全
     * 使用建议：
     *      CronTaskFuture类中使用的是ScheduledFuture对象来表示定时任务的执行结果。
     *      ScheduledFuture对象是线程安全的，因此不需要使用volatile关键字来保证多线程同步。
     *      如果需要在多线程中使用线程本地变量，可以使用ThreadLocal。
     *      因此，建议在CronTaskFuture类中使用ScheduledFuture对象，而不是使用volatile或ThreadLocal。
     *      另外，如果需要在Spring容器销毁时执行一些清理操作，可以实现DisposableBean接口，并在destroy()方法中进行清理操作。
     */
    public ScheduledFuture<?> future;
//    public volatile ScheduledFuture<?> future;
//    public void setThreadLocal(ScheduledFuture<?> future){
//        THREAD_LOCAL.set(future);
//    }

    /**
     * 取消当前定时任务
     */
    public void cancel() {
        try {
//            ScheduledFuture<?> future = THREAD_LOCAL.get();
            ScheduledFuture<?> future = this.future;
            if (Objects.nonNull(future)) {
                future.cancel(true);
            }
        }catch(Exception e){
            throw new RuntimeException("销毁定时任务失败！");
        }finally {
//            THREAD_LOCAL.remove();
        }

    }

}
