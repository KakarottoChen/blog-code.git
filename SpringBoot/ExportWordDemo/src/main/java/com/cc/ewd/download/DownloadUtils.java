package com.cc.ewd.download;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author CC
 * @since 2023/5/18 0018
 */
public class DownloadUtils {

    private static final Logger log = LoggerFactory.getLogger(DownloadUtils.class);

    /** 编码文件名
     * @param fileNames 设置的文件名
     * @param request HttpServletRequest
     * @return java.lang.String
     * @since 2023/5/18 0018
     * @author CC
     **/
    public static String encodeFileName(String fileNames, HttpServletRequest request) {
        String codedFilename = null;
        try {
            String agent = request.getHeader("USER-AGENT");
            // ie浏览器及Edge浏览器
            boolean as = null != agent && -1 != agent.indexOf("MSIE")
                      || null != agent && -1 != agent.indexOf("Trident")
                      || null != agent && -1 != agent.indexOf("Edge");
            if (as) {
                codedFilename = URLEncoder.encode(fileNames, StandardCharsets.UTF_8.name());
            } else if (null != agent && -1 != agent.indexOf("Mozilla")) {
                // 火狐,Chrome等浏览器
                codedFilename = new String(fileNames.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
            }else{
                codedFilename = fileNames;
            }

        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
        return codedFilename;
    }

    /** 标准下载的方法
     * @param response HttpServletResponse
     * @since 2023/5/18 0018
     * @author CC
     **/
    public void standardDownload(HttpServletRequest request, HttpServletResponse response){
        try {
            //构建好的需要下载的bytes数组
            byte[] bytes = new byte[1];
            String fileName = "文件名";
            String suffix = ".doc";

            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            response.setContentType("application/msword;charset=UTF-8");
            //Access-Control-Expose-Headers ：解决跨域不显示在header里面的问题
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS,HttpHeaders.CONTENT_DISPOSITION);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                    String.format("attachment; filename=%s%s", encodeFileName(fileName, request), suffix));
            ServletOutputStream out = response.getOutputStream();

            out.write(bytes);
            out.flush();
            out.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
