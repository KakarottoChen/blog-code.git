package com.cc.ewd.html;

/**
 * @author CC
 * @since 2023/4/24 0024
 */
public interface HtmlConstants {

    /**
     * 普通文档（富文本生成的）
     */
    String HTML1 = "<h1 style=\"text-align: center;\"><strong>文章标题</strong></h1><h1><strong>一、标题1" +
            "</strong></h1><p><strong> &nbsp; &nbsp; &nbsp; 我是数据：{NUM}</strong></p><h2><strong>" +
            "1.1、吾问无为谓</strong></h2><table style=\"width: 100%;\">" +
            "<tbody><tr><th colSpan=\"1\" rowSpan=\"1\" width=\"auto\">序号</th>" +
            "<th colSpan=\"1\" rowSpan=\"1\" width=\"auto\">第一列</th>" +
            "<th colSpan=\"1\" rowSpan=\"1\" width=\"auto\">第二列</th>" +
            "<th colSpan=\"1\" rowSpan=\"1\" width=\"auto\">第三列</th>" +
            "<th colSpan=\"1\" rowSpan=\"1\" width=\"auto\">第四列</th>" +
            "</tr><tr><td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">1</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">11</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">22</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">33</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">44</td>" +
            "</tr><tr><td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">2</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">11</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">22</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">33</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">44</td>" +
            "</tr><tr><td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">3</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">11</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">22</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">33</td>" +
            "<td colspan=\"1\" rowspan=\"1\" width=\"auto\" style=\"text-align: center;\">44</td></tr>" +
            "</tbody></table><p><br></p>";

    /**
     * 带表格文档（可以富文本生成，也可以使用word文档另存为HTML文件，然后拷出来）
     */
    String HTML2 = "<!--StartFragment--><div class=\"Section0\"  style=\"layout-grid:15.6000pt;\" ><h1 align=center  style=\"text-align:center;\" ><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;\n" +
            "font-size:22.0000pt;mso-font-kerning:22.0000pt;\" ><font face=\"宋体\" >标题</font></span></b><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;\n" +
            "font-size:22.0000pt;mso-font-kerning:22.0000pt;\" ><o:p></o:p></span></b></h1><h2><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:黑体;mso-ascii-font-family:Arial;\n" +
            "mso-hansi-font-family:Arial;mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;\n" +
            "font-size:16.0000pt;mso-font-kerning:1.0000pt;\" ><font face=\"黑体\" >一、段落</font><font face=\"Arial\" >1</font></span></b><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:Arial;mso-fareast-font-family:黑体;\n" +
            "mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;font-size:16.0000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></b></h2><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >哒哒</font></span><b><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';color:rgb(0,0,255);\n" +
            "font-weight:bold;font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >哒哒哒</font></span></b><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >哒</font></span><span style=\"mso-spacerun:'yes';font-family:Calibri;mso-fareast-font-family:宋体;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p><h3><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;\n" +
            "font-size:16.0000pt;mso-font-kerning:1.0000pt;\" ><font face=\"Calibri\" >1.1</font><font face=\"宋体\" >、表格</font></span></b><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:Calibri;mso-fareast-font-family:宋体;\n" +
            "mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;font-size:16.0000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></b></h3><table class=MsoTableGrid  border=1  cellspacing=0  style=\"border-collapse:collapse;border:none;mso-border-left-alt:0.5000pt solid windowtext;\n" +
            "mso-border-top-alt:0.5000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;mso-border-bottom-alt:0.5000pt solid windowtext;\n" +
            "mso-border-insideh:0.5000pt solid windowtext;mso-border-insidev:0.5000pt solid windowtext;mso-padding-alt:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;\" ><tr><td width=142  valign=center  style=\"width:106.5000pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:1.0000pt solid windowtext;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >序号</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td><td width=142  valign=center  style=\"width:106.5000pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:1.0000pt solid windowtext;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >列</font><font face=\"Calibri\" >1</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td><td width=142  valign=center  style=\"width:106.5500pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:1.0000pt solid windowtext;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >列</font><font face=\"Calibri\" >2</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td><td width=142  valign=center  style=\"width:106.5500pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:1.0000pt solid windowtext;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"宋体\" >列</font><font face=\"Calibri\" >3</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td></tr><tr><td width=142  valign=center  style=\"width:106.5000pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:none;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"Calibri\" >1</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td><td width=142  valign=center  style=\"width:106.5000pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:none;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"Calibri\" >11</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td><td width=142  valign=center  style=\"width:106.5500pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:none;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"Calibri\" >22</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td><td width=142  valign=center  style=\"width:106.5500pt;padding:0.0000pt 5.4000pt 0.0000pt 5.4000pt ;border-left:1.0000pt solid windowtext;\n" +
            "mso-border-left-alt:0.5000pt solid windowtext;border-right:1.0000pt solid windowtext;mso-border-right-alt:0.5000pt solid windowtext;\n" +
            "border-top:none;mso-border-top-alt:0.5000pt solid windowtext;border-bottom:1.0000pt solid windowtext;\n" +
            "mso-border-bottom-alt:0.5000pt solid windowtext;\" ><p class=MsoNormal  align=center  style=\"text-align:center;\" ><span style=\"font-family:宋体;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><font face=\"Calibri\" >33</font></span><span style=\"font-family:Calibri;mso-fareast-font-family:宋体;mso-bidi-font-family:'Times New Roman';\n" +
            "font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></p></td></tr></table><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:Calibri;mso-fareast-font-family:宋体;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p>&nbsp;</o:p></span></p><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:Calibri;mso-fareast-font-family:宋体;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p>&nbsp;</o:p></span></p><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:Calibri;mso-fareast-font-family:宋体;\n" +
            "mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;mso-font-kerning:1.0000pt;\" ><o:p>&nbsp;</o:p></span></p><h2><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:黑体;mso-ascii-font-family:Arial;\n" +
            "mso-hansi-font-family:Arial;mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;\n" +
            "font-size:16.0000pt;mso-font-kerning:1.0000pt;\" ><font face=\"黑体\" >二、段落</font><font face=\"Arial\" >2</font></span></b><b style=\"mso-bidi-font-weight:normal\" ><span style=\"mso-spacerun:'yes';font-family:Arial;mso-fareast-font-family:黑体;\n" +
            "mso-bidi-font-family:'Times New Roman';mso-ansi-font-weight:bold;font-size:16.0000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><o:p></o:p></span></b></h2><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><o:p>&nbsp;</o:p></span></p><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><o:p>&nbsp;</o:p></span></p><p class=MsoNormal ><span style=\"mso-spacerun:'yes';font-family:宋体;mso-ascii-font-family:Calibri;\n" +
            "mso-hansi-font-family:Calibri;mso-bidi-font-family:'Times New Roman';font-size:10.5000pt;\n" +
            "mso-font-kerning:1.0000pt;\" ><o:p>&nbsp;</o:p></span></p></div><!--EndFragment-->";

}
