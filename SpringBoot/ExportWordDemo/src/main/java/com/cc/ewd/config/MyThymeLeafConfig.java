package com.cc.ewd.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;

import javax.annotation.Resource;

/** ThymeLeaf单独的配置
 * @since 2023/5/4 0004
 * @author CC
 **/
@Configuration
public class MyThymeLeafConfig {

    @Resource
    private ApplicationContext applicationContext;

    /** 自定义的bean
     * @return SpringTemplateEngine
     * @Primary :<li>作用：指定使用名为“myTemplateEngine”的bean作为默认bean。</li>
     *          <li>这样，当您在需要使用SpringTemplateEngine的地方没有指定@Qualifier注释时，Spring将使用该默认bean。</li>
     *          <li>使用@Resource时，可直接设置名字。不用使用@Qualifier注释</li>
     */
    @Bean(name = "myTemplateEngine")
    @Primary
    public SpringTemplateEngine myTemplateEngine(){
        // SpringTemplateEngine自动应用SpringStandardDialect
        // 并启用Spring自己的MessageSource消息解析机制。
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        SpringResourceTemplateResolver templateResolver = myTemplateResolver();
        templateEngine.setTemplateResolver(templateResolver);
        // 使用Spring 4.2.4或更高版本启用SpringEL编译器
        // 可以加快大多数情况下的执行速度, 但是当一个模板中
        // 的表达式在不同数据类型之间重用时,
        // 可能与特定情况不兼容, 因此该标志默认为“false”
        // 以实现更安全的向后兼容性。
        templateEngine.setEnableSpringELCompiler(true);
        return templateEngine;
    }

    /** 自定义配置
     * @return SpringResourceTemplateResolver
     */
    @Bean("myTemplateResolver")
    public SpringResourceTemplateResolver myTemplateResolver(){
        // SpringResourceTemplateResolver自动与Spring自己集成
        // 资源解决基础设施, 强烈推荐。
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(this.applicationContext);
        templateResolver.setPrefix("classpath:thymeleafcs/");
        templateResolver.setSuffix(".html");
        templateResolver.setCharacterEncoding("UTF-8");
        // HTML是默认值, 为了清楚起见, 在此处添加。
        templateResolver.setTemplateMode(TemplateMode.HTML);
        return templateResolver;
    }

//----------------------------------------------

    /** 自定义的bean2
     * @return SpringTemplateEngine
     */
    @Bean(name = "myTemplateEngine2")
    public SpringTemplateEngine myTemplateEngine2(){
        // SpringTemplateEngine自动应用SpringStandardDialect
        // 并启用Spring自己的MessageSource消息解析机制。
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        SpringResourceTemplateResolver templateResolver = myTemplateResolver2();
        templateEngine.setTemplateResolver(templateResolver);
        // 使用Spring 4.2.4或更高版本启用SpringEL编译器
        // 可以加快大多数情况下的执行速度, 但是当一个模板中
        // 的表达式在不同数据类型之间重用时,
        // 可能与特定情况不兼容, 因此该标志默认为“false”
        // 以实现更安全的向后兼容性。
        templateEngine.setEnableSpringELCompiler(true);
        return templateEngine;
    }

    /** 自定义配置2
     * @return SpringResourceTemplateResolver
     */
    @Bean("myTemplateResolver2")
    public SpringResourceTemplateResolver myTemplateResolver2(){
        // SpringResourceTemplateResolver自动与Spring自己集成
        // 资源解决基础设施, 强烈推荐。
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(this.applicationContext);
        templateResolver.setPrefix("classpath:thymeleafcs/");
        templateResolver.setSuffix(".xml");
        templateResolver.setCharacterEncoding("UTF-8");
        // HTML是默认值, 为了清楚起见, 在此处添加。
        templateResolver.setTemplateMode(TemplateMode.XML);
        return templateResolver;
    }

}