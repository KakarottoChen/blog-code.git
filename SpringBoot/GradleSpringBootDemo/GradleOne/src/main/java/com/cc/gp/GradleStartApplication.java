package com.cc.gp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** <p>启动类<p>
 * @since 2023/9/20
 * @author CC
 **/
@SpringBootApplication
public class GradleStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(GradleStartApplication.class, args);
    }

}
