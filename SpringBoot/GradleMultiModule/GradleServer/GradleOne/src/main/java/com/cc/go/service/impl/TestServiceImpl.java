package com.cc.go.service.impl;

import com.cc.go.service.TestService;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/9/21
 */
@Service
@Data
public class TestServiceImpl implements TestService {
    /**
     * 测试模块是否被启动
     */
    @PostConstruct
    public void get1(){
        System.out.println("One模块启动。。。");
    }
}
