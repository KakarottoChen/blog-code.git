package com.cc.go.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

/** <p>自定义的拦截器<p>
 * @since 2023/9/21
 * @author CC
 **/
@Component
@Slf4j
public class MyHandlerInterceptor implements HandlerInterceptor {




}