package com.cc.jschdemo;

import com.cc.jschdemo.springmultiton.ISpringMultiton;
import com.cc.jschdemo.terminationThread.SpringThread;
import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.service.partnerpayments.jsapi.JsapiServiceExtension;
import com.wechat.pay.java.service.partnerpayments.jsapi.model.PrepayWithRequestPaymentResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

@SpringBootTest
class JSchDemoApplicationTests {

    String str1 = "h1e2l3l4o";
    String str2 = "ddddhello";

    //归一化编辑距离
    @Test
    void contextLoads() {
        // commons-text 包：根据编辑距离计算：相似度
        int editDistance = LevenshteinDistance.getDefaultInstance().apply(str1, str2);
        double similarity = 1 - ((double) editDistance / Math.max(str1.length(), str2.length()));

        System.out.println("commons-text 包：Edit Distance: " + editDistance);
        System.out.println("commons-text 包：Similarity: " + similarity);
    }

    //余弦相似度
    @Test
    public void test02()throws Exception{
        // commons-text 包
        // 使用Cosine计算两个字符串的余弦距离
        CosineDistance cd = new CosineDistance();
        Double apply = cd.apply(str2, str1);
        System.out.println("Cosine相似度：" + apply);
    }

    //Jaro-Winkler相似度
    @Test
    public void test03()throws Exception{
        JaroWinklerSimilarity js = new JaroWinklerSimilarity();
        System.out.println("Jaro-Winkler相似度: " + js.apply(str1, str2));
    }

    @Resource
    private ISpringMultiton springMultiton;

    //默认是单例的
    @Test
    public void test04()throws Exception{
        int i = springMultiton.hashCode();
        System.out.println("第一次使用的地址：" + i);


        int i1 = springMultiton.hashCode();
        System.out.println("第二次使用的地址：" + i1);
    }

    @Resource
    private ObjectProvider<ISpringMultiton> objectProvider;

    @Test
    public void test05()throws Exception{
        //这里的objectProvider.getObject()获取出来的，就相当于spring注入的ISpringMultiton
        ISpringMultiton springMultiton1 = objectProvider.getObject();
        int i = springMultiton1.hashCode();
        System.out.println("第一次使用的地址：" + i);

        ISpringMultiton springMultiton2 = objectProvider.getObject();
        int i1 = springMultiton2.hashCode();
        System.out.println("第二次使用的地址：" + i1);
    }

    @Test
    public void test06()throws Exception{
        //t1：模仿
        Thread t1 = new Thread(() -> {
            try {
                //任务1
                Thread.sleep(1000);
                System.out.println("完成：任务1");

                //任务2（模仿阻塞）
                while (true) {
                    Thread.sleep(1000);
                    System.out.println("完成：任务2");
                }
            }catch (InterruptedException e) {
                e.printStackTrace();
                //捕获异常：InterruptedException，然后打断
                //☆☆打断，终止当前线程☆☆
                Thread.currentThread().interrupt();
            }
        });

        //t2：模仿其他操作，打断线程
        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(3000);
                System.out.println("打断线程！");
                //打断t1线程
                t1.interrupt();
            }catch (Exception e) {
                e.printStackTrace();
            }
        });

        //启动两个线程
        t1.start();
        t2.start();
    }

    //可以直接注入线程
    @Resource
    private SpringThread springThread;

    //模仿创建任务、打断
    @Test
    public void test07()throws Exception{
        try {
            ///传入执行任务，需要的参数
            springThread.setFootwork("步奏");
            //启动线程
            springThread.start();

            Thread.sleep(3100);
            System.out.println("打断线程！");
            //打断t1线程
            springThread.interrupt();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    //模仿任务线程，也可以是：Runnable
    Thread task1 = new Thread(()->{
        try {
            //检查终止状态
            boolean interrupted = Thread.currentThread().isInterrupted();
            //模仿线程阻塞
            while (!interrupted) {
                System.out.println("任务1执行...");
                Thread.sleep(1000);
            }
        }catch(Exception e){
            //发送异常，进行打断当前线程
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    });
    Thread task2 = new Thread(()->{
        try {
            //模仿线程阻塞
            while (true) {
                System.out.println("任务2执行...");
                Thread.sleep(1000);
            }
        }catch(Exception e){
            //发送异常，进行打断当前线程
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    });

    @Test
    public void test08()throws Exception{
        //实际使用中，可以使用循环添加CompletableFuture，然后记录下future（缓存），在要中断的逻辑中调用cancel
        //1开启task任务
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(task1);
        CompletableFuture<Void> future2 = CompletableFuture.runAsync(task2);

        //2先让task跑3s后再中断
        try {
            Thread.sleep(3000);
        }catch(Exception e){
            e.printStackTrace();
        }
        //3取消任务
        // 参数true表示尝试中断任务执行
        // cancel方法会尝试取消任务的执行，参数true表示尝试中断任务执行。
        // 成功取消返回true，否则返回false。
        // 需要注意的是，cancel方法并不会直接中断正在执行的线程，而是会尝试取消任务的执行，
        // 具体是否成功取决于任务的实现。
        boolean cancel1 = future1.cancel(Boolean.TRUE);
//        boolean cancel2 = future2.cancel(Boolean.TRUE);
        System.out.println("任务1终止：" + (cancel1 ? "成功1" : "失败1"));
//        System.out.println("任务2终止：" + (cancel2 ? "成功2" : "失败2"));

        //4阻塞全部线程，如果不执行第3步，线程会一直执行到地球爆炸
        // 经过测试，必须两个future都终止，才会终止完，否则线程还是会一直跑任务。
        future1.join();
        future2.join();


    }


    /** 商户号 */
    private static String merchantId = "1639882527";
    // private static String merchantId = "6FC33FEED5B818D94F568C32342D914A169F04B6";
    private static String appId = "wxdea7be0a253f3c63";
    /** 商户API私钥路径 */
// private static String privateKeyPath = "zhifu/apiclient_key.pem";
//    private static String privateKeyPath = "D:\\Develop\\blog-code\\SpringBoot\\JSchDemo\\src\\main\\resources\\pudao\\apiclient_key.pem";
    private static String privateKeyPath = "./src/main/resources/pudao/apiclient_key.pem";
    /** 商户证书序列号 */
    private static String merchantSerialNumber = "6FC33FEED5B818D94F568C32342D914A169F04B6";
    /** 商户APIV3密钥 */
    private static String apiV3Key = "https_pay_weixin_qq_com_djkn_624";


    private JsapiServiceExtension getWXService() {
// 使用自动更新平台证书的RSA配置
// 一个商户号只能初始化一个配置，否则会因为重复的下载任务报错
        Config config = new RSAAutoCertificateConfig.Builder()
                .merchantId(merchantId)
                .privateKeyFromPath(privateKeyPath)
                .merchantSerialNumber(merchantSerialNumber)
                .apiV3Key(apiV3Key)
                .build();
        return null;
        
//        return new JsapiServiceExtension.Builder()
//                .config(config)
//                .signType("RSA") // 不填默认为RSA
//                .build();
    }

    @Test
    public void test11()throws Exception{
        getWXService();


    }








}
