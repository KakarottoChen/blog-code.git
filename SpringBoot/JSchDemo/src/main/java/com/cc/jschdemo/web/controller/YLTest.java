package com.cc.jschdemo.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p></p>
 *
 * @author --
 * @since 2024/1/9
 */
@RestController
@RequestMapping("/yaLi")
public class YLTest {

    @GetMapping
    public String get(){
        String msg = "我是压力测试的接口！！！";
        System.out.println(msg + "——" + Thread.currentThread().getName());
        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return msg;
    }


}
