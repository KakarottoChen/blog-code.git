package com.cc.jschdemo;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTString;

import java.io.FileInputStream;
import java.util.List;
import java.util.Objects;

/**
 * <p>读取word文档</p>
 *
 * @author cc
 * @since 2023/11/24
 */
public class ReadWord {

    public static void main(String[] args) {
        try (
            FileInputStream fis = new FileInputStream("D:\\Develop\\Test\\导入图片Word文档.docx");
            XWPFDocument document = new XWPFDocument(fis);
            ) {

//            WordExtractor ex = new WordExtractor(fis);
            List<XWPFParagraph> paragraphs = document.getParagraphs();
            boolean check = Boolean.FALSE;
            for (XWPFParagraph paragraph : paragraphs) {
                // 检查段落是否为三级标题
                String style = paragraph.getStyle();

                CTP ctp = paragraph.getCTP();
                if (Objects.nonNull(ctp)) {
                    CTPPr pPr = ctp.getPPr();
                    if (Objects.nonNull(pPr)) {
                        CTString pStyle = pPr.getPStyle();
                        if (Objects.nonNull(pStyle)) {
                            String val = pStyle.getVal();
                            if ("3".equals(val)) {
                                System.out.println(paragraph.getText());
                                check = Boolean.TRUE;
                            }
                        }

                    }
                }
                //开始检查图片
                if (check) {
                    List<XWPFRun> runs = paragraph.getRuns();
                    for (XWPFRun run : runs) {
                        List<XWPFPicture> pictures = run.getEmbeddedPictures();
                        if (!pictures.isEmpty()) {
                            System.out.println("该段落包含图片");
                            for (XWPFPicture picture : pictures) {
                                System.out.println("图片: " + picture.getPictureData().getFileName());
                            }
                        }
                    }
                }

            }

            /*List<IBodyElement> bodyElements = document.getBodyElements();
            boolean collectPictures = false;
            for (IBodyElement bodyElement : bodyElements) {
                if (bodyElement instanceof XWPFParagraph) {
                    XWPFParagraph paragraph = (XWPFParagraph) bodyElement;
                    // 检查段落是否为三级标题
                    CTP ctp = paragraph.getCTP();
                    if (Objects.nonNull(ctp) && ctp.getPPr().getPStyle().getVal().equals("3")) {
                        System.out.println("三级标题: " + paragraph.getText());
                        collectPictures = true;
                    } else if (paragraph.getCTP().getPPr().getPStyle().getVal().startsWith("Heading")) {
                        collectPictures = false;
                    }

                    if (collectPictures) {
                        // 获取该标题下的所有图片
                        List<XWPFPictureData> pictures = paragraph.getRuns().stream()
                                .flatMap(run -> run.getEmbeddedPictures().stream())
                                .map(XWPFPicture::getPictureData)
                                .collect(Collectors.toList());
                        for (XWPFPictureData picture : pictures) {
                            System.out.println("图片: " + picture.getFileName());
                        }
                    }
                }
            }*/

        }catch(Exception e){
            e.printStackTrace();
        }




    }



}
