package com.cc.jschdemo.springmultiton;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * <p></p>
 *
 * @author --
 * @since 2023/11/23
 */
@Component
@Scope("prototype")
public class SpringMultitonImpl implements ISpringMultiton{



}
