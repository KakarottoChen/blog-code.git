package com.cc.jschdemo.terminationThread;

/**
 * <p>基本打断</p>
 *
 * @author cc
 * @since 2023/11/23
 */
public class BasicInterrupt {

    /**
     * 终止
     * @param args
     */
    public static void main(String[] args) {
        //t1：模仿执行任务的线程
        Thread t1 = new Thread(() -> {
            try {
                //步奏1
                Thread.sleep(1000);
                System.out.println("完成：步奏1");

                //步奏2（模仿阻塞）
                while (true) {
                    Thread.sleep(1000);
                    System.out.println("完成：步奏2");
                }
            }catch (InterruptedException e) {
                e.printStackTrace();
                //捕获异常：InterruptedException，然后打断
                //☆☆打断，终止当前线程☆☆
                Thread.currentThread().interrupt();
            }
        });

        //t2：模仿其他操作，打断线程
        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(3000);
                System.out.println("打断线程！");
                //打断t1线程
                t1.interrupt();
            }catch (Exception e) {
                e.printStackTrace();
            }
        });

        //启动两个线程
        t1.start();
        t2.start();
    }

}
