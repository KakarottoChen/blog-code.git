package com.cc.mdb;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 属性：(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) 是因为集成了WebSocket
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BaseTest {

}