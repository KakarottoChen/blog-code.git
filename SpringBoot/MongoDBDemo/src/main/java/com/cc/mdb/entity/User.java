package com.cc.mdb.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

/**
 * @Document("User") 存入的时候不取对象名，默认用User当做表名
 */
@Data
@Document("User")
public class User {

    @Id
    private String id;
    /**
     * 使用name当做字段key
     */
    @Field("name")
    private String name;
    /**
     * 使用age1当做字段key
     */
    @Field("age1")
    private Integer age;
    /**
     * 使用email当做字段key
     */
    @Field("email")
    private String email;
    /**
     * 使用createDate当做字段key
     */
    @Field("createDate")
    private LocalDateTime createDate = LocalDateTime.now();
}

