package com.cc.xiuxidemo.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author CC
 * @since 2023/2/10 0010
 */
@RequestMapping("/xiuxi")
public class XiuxiDemoController {

    @GetMapping
    public String xiuxi(){

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleWithFixedDelay(()->{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                System.out.println("开始休息吧   "+ sdf.format(new Date()));
                Runtime.getRuntime().exec("RunDll32.exe user32.dll,LockWorkStation");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("-------------------请输入启动命令-------------------");
            String next = scanner.next();
            System.out.println("倒计时开始，不要太辛苦哦   "+ sdf.format(new Date()));
        },3,1 * 60 * 60, TimeUnit.SECONDS);

        return "OK";
    }


}
