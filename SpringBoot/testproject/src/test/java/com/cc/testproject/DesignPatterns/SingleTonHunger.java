package com.cc.testproject.DesignPatterns;

/** 单例模式：饿汉
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/26 15:17
 * @Version 1.0
 */
public class SingleTonHunger {

    private static final SingleTonHunger singleTonHunger = new SingleTonHunger();

    private SingleTonHunger() {
    }

    public static SingleTonHunger getInstance(){
        return singleTonHunger;
    }
}
