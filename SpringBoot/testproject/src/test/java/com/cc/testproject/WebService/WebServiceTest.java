package com.cc.testproject.WebService;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cc.testproject.BaseTest;
import com.cc.testproject.util.DesEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.jupiter.api.Test;

import javax.xml.namespace.QName;

/**
 * @Description 测试访问 鼎通 的 WebService接口
 * @Author ChenCheng
 * @Date 2021/7/29 12:34
 * @Version 1.0
 */
@Slf4j
public class WebServiceTest extends BaseTest {


    @Test
    public void test01() throws Exception {
        JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient("http://47.92.223.245:7007/mls/webService/dataService?wsdl");
        // namespaceURI 是命名空间,在wsdl地址中，需要访问查看
        // methodName是方法名，我们要调用的方法
        QName name = new QName(
                "http://dataservice.webService.service.mls.hac.com",
                "getDeviceCurrentData");
        Object[] objects;
        try {
            // 第一个参数是上面的QName，第二个开始为参数，可变数组
            objects = client.invoke(name, "yagr","62BC9utKW20=","",500,1);
            System.out.println("对象地址是:"+objects);

            JSONObject jsonObject = JSON.parseObject(objects[0].toString());
            System.out.println("转换为"+jsonObject);

//            JSONArray data = JSON.parseArray(JSON.toJSONString(jsonObject.get("data")));
//            System.out.println(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @Description 测试访问鼎通水表信息
     * @Author ChenCheng
     * @Date 2021/7/31 16:13
     **/
    @Test
    public void test02()throws Exception{
        JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient("http://47.92.223.245:7007/mls/webService/dataService?wsdl");
        // namespaceURI 是命名空间,在wsdl地址中，需要访问查看
        // methodName是方法名，我们要调用的方法
        QName name = new QName(
                "http://dataservice.webService.service.mls.hac.com",
                "getDeviceInfo");
        Object[] objects;
        try {
            // 第一个参数是上面的QName，第二个开始为参数，可变数组
            Object[] objects1 = {"yagr","62BC9utKW20=","20010630","20210731","",500,1};
            objects = client.invoke(name,objects1);
            System.out.println("对象地址是:"+objects);

            JSONObject jsonObject = JSON.parseObject(objects[0].toString());
            System.out.println("转换为"+jsonObject);

//            JSONArray data = JSON.parseArray(JSON.toJSONString(jsonObject.get("data")));
//            System.out.println(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @Description 测试CBC加密
     * @Author ChenCheng
     * @Date 2021/7/31 16:12
     **/
    @Test
    public void test03()throws Exception{
        DesEncoder desEncoder = new DesEncoder();
//        String s = desEncoder.CBCDecrypt("62BC9utKW20=");
//        System.out.println(s);
        String s1 = desEncoder.CBCEncrypt("111111");
        System.out.println(s1);
    }



}
