package com.cc.testproject.web.controller;

import com.cc.testproject.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/2 11:00
 * @Version 1.0
 */
@Slf4j
public class StartControllerTest extends BaseTest {


    @Test
    public void test01()throws Exception{
        MvcResult mvcResult = mockMvc.perform(get("/start1/get/参数1/参数2"))
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.id").value(1))//使用json path表达式
                .andDo(print())
                .andReturn();
        Object handler = mvcResult.getHandler();
        log.info("测试get结果是：{}",handler);
    }


}
