package com.cc.testproject.cron;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

/** 生成Cron工具类
 * @Description
 * @Author CC
 * @Date 2021/10/22
 **/
@Slf4j
public class CronUtil {

    /**
     *方法摘要：构建Cron表达式
     */
    public static String createCronExpression(TaskScheduleModel taskScheduleModel){
        /**所选作业类型:
         * 1  -> 每秒
         * 2  -> 每分
         * 3  -> 每时
         * 4  -> 每天
         * 5  -> 每周
         * 6  -> 每月
         */
        StringBuilder cronExp = new StringBuilder("");
        Integer jobType = taskScheduleModel.getJobType();
        if(jobType!=1 && jobType!=2 && jobType!=3 && jobType!=4 && jobType!=5 && jobType!=6 ) {
            log.error("执行类型只能是1/2/3/4/5/6（秒/分/时/天/周/月）");
        }
        //1-每隔几秒
        if (jobType == 1) {
            cronExp.append("0/").append(taskScheduleModel.getSecond());
            cronExp.append(" ");
            cronExp.append("* ");
            cronExp.append("* ");
            cronExp.append("* ");
            cronExp.append("* ");
            cronExp.append("?");
            return cronExp.toString();
        }
        //2-每隔几分钟
        if (jobType == 2) {
            cronExp.append("0 ");
            cronExp.append("0/").append(taskScheduleModel.getMinute());
            cronExp.append(" ");
            cronExp.append("* ");
            cronExp.append("* ");
            cronExp.append("* ");
            cronExp.append("?");
            return cronExp.toString();
        }
        //3-每隔几小时
        if (jobType == 3) {
            cronExp.append("0 ");
            cronExp.append("0 ");
            cronExp.append("0/").append(taskScheduleModel.getHour());
            cronExp.append(" ");
            cronExp.append("* ");
            cronExp.append("* ");
            cronExp.append("?");
            return cronExp.toString();
        }
//-------------------每天、或每周、或每月哪天执行-------------------
        //秒
        cronExp.append(taskScheduleModel.getSecond()).append(" ");
        //分
        cronExp.append(taskScheduleModel.getMinute()).append(" ");
        //小时
        cronExp.append(taskScheduleModel.getHour()).append(" ");
        //4-每天
        if(jobType == 4){
            cronExp.append("* ");//日
            cronExp.append("* ");//月
            cronExp.append("?");//周
        }
        //5-按每周
        else if(jobType == 5){
            //一个月中第几天
            cronExp.append("? ");
            //月份
            cronExp.append("* ");
            //周
            List<Integer> weekR = Lists.newArrayList();

            List<Integer> weeks = taskScheduleModel.getDayOfWeeks();
            Assert.isTrue(weeks.size() > 0,"必须传入周几！");
            List<Integer> week = Lists.newArrayList(1,2,3,4,5,6,7);
            weeks.forEach(integer -> Assert.isTrue(week.contains(integer),"星期只能是1-7"));
            List<Integer> collect1 = weeks.stream().filter(execWeek -> execWeek != 7).map(execWeek -> execWeek + 1).collect(Collectors.toList());
            List<Integer> collect = weeks.stream().filter(execWeek -> execWeek == 7).map(execWeek -> execWeek = 1).collect(Collectors.toList());
            weekR.addAll(collect1);
            weekR.addAll(collect);

            for(int i = 0; i < weekR.size(); i++){
                if(i == 0){
                    cronExp.append(weekR.get(i));
                } else{
                    cronExp.append(",").append(weekR.get(i));
                }
            }
        }
        //6-按每月
        else if(jobType == 6){
            //一个月中的哪几天
            List<Integer> days = taskScheduleModel.getDayOfMonths();
            Assert.isTrue(days.size() > 0,"必须传入月份的天(1-31)！");
            List<Integer> month = Lists.newArrayList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31);
            days.forEach(integer -> Assert.isTrue(month.contains(integer),"月份的天只能是1-31"));
            for(int i = 0; i < days.size(); i++){
                if(i == 0){
                    cronExp.append(days.get(i));
                } else{
                    cronExp.append(",").append(days.get(i));
                }
            }
            //月份
            cronExp.append(" * ");
            //周
            cronExp.append("?");
        }
        return cronExp.toString();
    }

    /**
     *方法摘要：生成计划的详细描述
     *@param  taskScheduleModel
     *@return String
     */
    public static String createDescription(TaskScheduleModel taskScheduleModel){
        StringBuffer description = new StringBuffer("");
        //计划执行开始时间
//      Date startTime = taskScheduleModel.getScheduleStartTime();

        if (null != taskScheduleModel.getSecond()
                && null != taskScheduleModel.getMinute()
                && null != taskScheduleModel.getHour()) {
            //按每天
            if(taskScheduleModel.getJobType().intValue() == 4){
                description.append("每天");
                description.append(taskScheduleModel.getHour()).append("时");
                description.append(taskScheduleModel.getMinute()).append("分");
                description.append(taskScheduleModel.getSecond()).append("秒");
                description.append("执行");
            }

            //按每周
            else if(taskScheduleModel.getJobType().intValue() == 5){
                if(taskScheduleModel.getDayOfWeeks() != null && taskScheduleModel.getDayOfWeeks().size() > 0) {
                    String days = "";
                    for(int i : taskScheduleModel.getDayOfWeeks()) {
                        days += "周" + i;
                    }
                    description.append("每周的").append(days).append(" ");
                }
                if (null != taskScheduleModel.getSecond()
                        && null != taskScheduleModel.getMinute()
                        && null != taskScheduleModel.getHour()) {
                    description.append(",");
                    description.append(taskScheduleModel.getHour()).append("时");
                    description.append(taskScheduleModel.getMinute()).append("分");
                    description.append(taskScheduleModel.getSecond()).append("秒");
                }
                description.append("执行");
            }

            //按每月
            else if(taskScheduleModel.getJobType().intValue() == 6){
                //选择月份
                if(taskScheduleModel.getDayOfMonths() != null && taskScheduleModel.getDayOfMonths().size() > 0) {
                    String days = "";
                    for(int i : taskScheduleModel.getDayOfMonths()) {
                        days += i + "号";
                    }
                    description.append("每月的").append(days).append(" ");
                }
                description.append(taskScheduleModel.getHour()).append("时");
                description.append(taskScheduleModel.getMinute()).append("分");
                description.append(taskScheduleModel.getSecond()).append("秒");
                description.append("执行");
            }

        }
        return description.toString();
    }

    //参考例子
    public static void main(String[] args) {
        //执行时间：每天的12时12分12秒 start
        TaskScheduleModel taskScheduleModel = new TaskScheduleModel();

        taskScheduleModel.setJobType(1);//按每秒
        taskScheduleModel.setSecond(30);
        String cronExp1 = createCronExpression(taskScheduleModel);
        System.out.println(cronExp1);

        taskScheduleModel.setJobType(2);//按每分钟
        taskScheduleModel.setMinute(10);
        String cronExp2 = createCronExpression(taskScheduleModel);
        System.out.println(cronExp2);

        taskScheduleModel.setJobType(3);//按小时
        taskScheduleModel.setHour(2);
        String cronExp3 = createCronExpression(taskScheduleModel);
        System.out.println(cronExp3);

        taskScheduleModel.setJobType(4);//按每天
        Integer hour = 11; //时
        Integer minute = 20; //分
        Integer second = 50; //秒
        taskScheduleModel.setHour(hour);
        taskScheduleModel.setMinute(minute);
        taskScheduleModel.setSecond(second);
        //
        String cropExp = createCronExpression(taskScheduleModel);
        System.out.println(cropExp + ":" + createDescription(taskScheduleModel));
        //执行时间：每天的12时12分12秒 end

        taskScheduleModel.setJobType(5);//每周的哪几天执行
        List<Integer> dayOfWeeks = Lists.newArrayList(6,7);
        taskScheduleModel.setDayOfWeeks(dayOfWeeks);
        cropExp = createCronExpression(taskScheduleModel);
        System.out.println(cropExp + ":" + createDescription(taskScheduleModel));

        taskScheduleModel.setJobType(6);//每月的哪几天执行
        List<Integer> dayOfMonths = Lists.newArrayList(31);
        taskScheduleModel.setDayOfMonths(dayOfMonths);
        cropExp = createCronExpression(taskScheduleModel);
        System.out.println(cropExp + ":" + createDescription(taskScheduleModel));

    }

}