package com.cc.testproject.hutool.StrUtil;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import org.apache.logging.log4j.message.SimpleMessage;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author CC
 * @Date 2021/12/11
 * @Version 1.0
 */
public class StrUtilTest extends BaseTest {

    @Test
    public void test3()throws Exception{
        System.out.println(StrUtil.join("-", "a", "b", DateUtil.current()));
    }

    //isNumeric 判断字符串是否是纯正整数组成
    @Test
    public void test01isNumeric()throws Exception{
        //只能判断正整数
        System.out.println(StrUtil.isNumeric("111"));     //true
        System.out.println(StrUtil.isNumeric("0111"));    //true
        System.out.println(StrUtil.isNumeric("1101"));    //true
        System.out.println(StrUtil.isNumeric("111.11"));  //false
        System.out.println(StrUtil.isNumeric("-111"));    //false
        System.out.println(StrUtil.isNumeric("111 "));    //false
        System.out.println(StrUtil.isNumeric("1 11"));    //false
        System.out.println(StrUtil.isNumeric("11 1"));    //false
        System.out.println(StrUtil.isNumeric(" 111"));    //false
    }

    @Test
    public void test02()throws Exception{
        // 移除字符串中所有给定字符串，当某个字符串出现多次，则全部移除
        // removeAny  例：removeAny("aa-bb-cc-dd", "a", "b") =》 --cc-dd
        System.out.println(StrUtil.removeAny("aa-bb-cc-dd-aa-bb-tt","aa-bb","bb-tt"));//-cc-dd--tt
        System.out.println(StrUtil.removeAny("aa-bb-cc-dd-aa-bb-tt","-"));//aabbccddaabbtt
        /* replace
         *     @param str         字符串
         *     @param searchStr   被查找的字符串
         *     @param replacement 被替换的字符串
         * aa-mm-cc-dd-aa-mm-tt
         **/
        System.out.println(StrUtil.replace("aa-bb-cc-dd-aa-bb-tt","b","m"));
        //wrapIfMissing  包装指定字符串，如果前缀或后缀已经包含对应的字符串，则不再包装
        System.out.println(StrUtil.wrapIfMissing("-abc","-","\""));  //结果：-abc"
        //equals
        System.out.println("A".equalsIgnoreCase("a"));  //true
        System.out.println(StrUtil.equals("A","a"));                //false
        System.out.println(StrUtil.equalsIgnoreCase("A","a"));      //true
        //count 统计字符串中指定字符的个数
        System.out.println(StrUtil.count("abcabc","a"));//2
        //contains 查看字符串是否包含在str字符串中
        System.out.println(StrUtil.contains("ABCabca","ca"));//true
        System.out.println(StrUtil.contains("ABCabca","AAA"));//false
        //containsAnyIgnoreCase 查看字符串是否包含在str字符串中 - 忽略大小写
        System.out.println(StrUtil.containsAnyIgnoreCase("ABCDDabca","dd"));//true
        //查看字符串是否是指定字符串开头
        System.out.println(StrUtil.startWith("-ss-","-"));//true
        System.out.println(StrUtil.startWith("ss-","-"));//false
        System.out.println(StrUtil.startWith("-ss-","-s"));//true
        // 查看字符串是否是指定字符串结尾
        System.out.println(StrUtil.endWith("-ss-","-"));//true
        System.out.println(StrUtil.endWith("ss-","s"));//false
        System.out.println(StrUtil.endWith("-ss-","s-"));//true

        String str = "aaabbbxxxccc111";
        System.out.println(StrUtil.subPre(str,str.indexOf("ccc111")));//截取指定字符串之前的字符串
        String aaabbb = StrUtil.subAfter(str, "aaabbb", true);
        System.out.println(aaabbb);//截取指定字符串之前的字符串
        System.out.println(StrUtil.subPre(aaabbb,aaabbb.indexOf("ccc111")));//截取指定字符串之前的字符串
    }

    @Test
    public void test03()throws Exception{
        Map<String,String> map = new HashMap<>();
        map.put("id","1111");
        Uuu info = Uuu.info(map);
        System.out.println(info);
        Object data = info.getData();
        System.out.println(data);
        Map<String,String> map1 = (Map) data;
        System.out.println(map1.get("id"));

        Map<String, Object> map2 = BeanUtil.beanToMap(data);
        System.out.println(map2.get("id"));
    }


}
