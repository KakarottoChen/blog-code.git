package com.cc.testproject.springScheduled;

import com.cc.testproject.BaseTest;
import com.cc.testproject.springScheduled.config.CronTaskRegistrar;
import com.cc.testproject.springScheduled.task.SchedulingRunnable;
import org.junit.Test;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.concurrent.ScheduledFuture;

/**
 * @Description
 * @Author CC
 * @Date 2021/11/2
 * @Version 1.0
 */
public class SpringScheduledTest extends BaseTest {

    @Resource
    private CronTaskRegistrar cronTaskRegistrar;

    /** 测试自定义cron表达式，无参
     * @Description
     * @Author CC
     * @Date 2021/11/2
     * @Param []
     * @Return void
     **/
    @Test
    public void testTask1() throws InterruptedException {
        SchedulingRunnable task = new SchedulingRunnable(
                "demoTask",
                "taskNoParams");
        cronTaskRegistrar.addCronTask(task, "0/5 * * * * ?");
        // 便于观察
        Thread.sleep(200000);
        // 移除定时任务
        cronTaskRegistrar.removeCronTask(task);
    }

    /** 测试自定义cron表达式，有参
     * @Description
     * @Author CC
     * @Date 2021/11/2
     * @Param []
     * @return void
     **/
    @Test
    public void testTask2() throws InterruptedException {
        SchedulingRunnable task = new SchedulingRunnable(
                "demoTask",
                "taskNoParams",
                "1");
        cronTaskRegistrar.addCronTask(task,"0/5 * * * * ?");
        // 便于观察
        Thread.sleep(200000);
        // 移除定时任务
        cronTaskRegistrar.removeCronTask(task);
    }


    //上面两个是复杂的，下面的是简单的
    @Resource
    private TaskScheduler taskScheduler;

    @Test
    public void test03()throws Exception{
        //就是想办法把 taskRunnable 和 future 记录下来，然后关闭
        TaskRunnable01 taskRunnable = new TaskRunnable01();
        ScheduledFuture<?> future = taskScheduler.schedule(taskRunnable, new CronTrigger("0/5 * * * * ?"));
        Thread.sleep(200000);
        boolean cancel = future.cancel(true);
    }

    public static class TaskRunnable01 implements Runnable{
        @Override
        public void run() {
            System.out.println("我是任务！" + LocalDateTime.now());
        }
    }


}
