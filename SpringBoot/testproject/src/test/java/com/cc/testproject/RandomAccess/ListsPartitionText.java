package com.cc.testproject.RandomAccess;

import com.cc.testproject.BaseTest;
import com.google.common.collect.Lists;
import org.junit.Test;
import java.util.List;


/**
 * @Description
 * @Author CC
 * @Date 2021/10/25
 * @Version 1.0
 */
public class ListsPartitionText extends BaseTest {


    /**
     * 对子集合的操作会反映到原集合， 对原集合的操作也会影响子集合
     * so:所以在给使用这个方法的时候，
     */
    @Test
    public void test01()throws Exception{
        List<Integer> numList = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8);
        List<List<Integer>> listss = Lists.partition(numList,3);
        for (List<Integer> lists : listss) {
            lists.add(9);
            System.out.println("里层集合："+lists);
        }
        System.out.println("外层集合："+listss);  //[[1, 2, 3], [4, 5, 6], [7, 8]]

    }



}
