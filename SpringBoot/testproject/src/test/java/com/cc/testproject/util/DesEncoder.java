package com.cc.testproject.util;

import java.io.UnsupportedEncodingException;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

public class DesEncoder {
	private final static String HASH_ALGORITHM = "SHA1";
	private String shareKey;
	private byte[] byKey;
	private byte[] byIV;

	public DesEncoder() throws Exception {
		this.setShareKey(this.generateNewShareKey());
	}

	public DesEncoder(String shareKey) throws Exception {
		setShareKey(shareKey);
	}

	/**
	 * 生成密钥
	 * 
	 * @return
	 */
	private byte[] generateKey() {
		try {
			// DES算法要求有一个可信任的随机数源
			SecureRandom sr = new SecureRandom();
			// 生成一个DES算法的KeyGenerator对象
			KeyGenerator kg = KeyGenerator.getInstance("DES");
			kg.init(sr);
			// 生成密钥
			SecretKey secretKey = kg.generateKey();
			// 获取密钥数据
			byte[] key = secretKey.getEncoded();
			return key;
		} catch (NoSuchAlgorithmException e) {
			System.err.println("DES算法，生成密钥出错!");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 生成新共享密钥
	 * 
	 * @return
	 */
	public String generateNewShareKey() {
		BASE64Encoder base64en = new BASE64Encoder();
		try {
			return base64en.encode(generateKey());
		} finally {
			base64en = null;
		}
	}

	/**
	 * 加密函数
	 * 
	 * @param data
	 * @return
	 */
	public String encrypt(String data) {
		BASE64Encoder base64en = new BASE64Encoder();
		try {
			// DES算法要求有一个可信任的随机数源
			SecureRandom sr = new SecureRandom();
			// 从原始密钥数据创建DESKeySpec对象
			DESKeySpec dks = new DESKeySpec(byKey);
			// 创建一个密匙工厂，然后用它把DESKeySpec转换成
			// 一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);
			// using DES in ECB mode
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			// 用密匙初始化Cipher对象
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, sr);
			// 执行加密操作
			byte encryptedData[] = cipher.doFinal(data.getBytes("UTF8"));
			return base64en.encode(encryptedData);
		} catch (Exception e) {
			System.err.println("DES算法，加密数据出错!");
			e.printStackTrace();
		} finally {
			base64en = null;
		}
		return null;
	}

	/**
	 * 解密函数
	 * 
	 * @param data
	 * @return
	 */
	public String decrypt(String data) {
		BASE64Decoder base64de = new BASE64Decoder();
		try {
			// DES算法要求有一个可信任的随机数源
			SecureRandom sr = new SecureRandom();
			// byte rawKeyData[] = /* 用某种方法获取原始密匙数据 */;
			// 从原始密匙数据创建一个DESKeySpec对象
			DESKeySpec dks = new DESKeySpec(byKey);
			// 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
			// 一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);
			// using DES in ECB mode
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			// 用密匙初始化Cipher对象
			cipher.init(Cipher.DECRYPT_MODE, secretKey, sr);
			// 正式执行解密操作
			byte decryptedData[] = cipher.doFinal(base64de.decodeBuffer(data));
			return new String(decryptedData, "UTF8");
		} catch (Exception e) {
			System.err.println("DES算法，解密出错。");
			e.printStackTrace();
		} finally {
			base64de = null;
		}
		return null;
	}

	/**
	 * CBC加密函数
	 * 
	 * @param data
	 * @return
	 */
	public String CBCEncrypt(String data) {
		BASE64Encoder base64en = new BASE64Encoder();
		try {
			// 从原始密钥数据创建DESKeySpec对象
			DESKeySpec dks = new DESKeySpec(byKey);
			// 创建一个密匙工厂，然后用它把DESKeySpec转换成
			// 一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);
			// Cipher对象实际完成加密操作
			Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
			// 若采用NoPadding模式，data长度必须是8的倍数
			// Cipher cipher = Cipher.getInstance("DES/CBC/NoPadding");
			// 用密匙初始化Cipher对象
			IvParameterSpec param = new IvParameterSpec(byIV);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, param);
			// 执行加密操作
			byte encryptedData[] = cipher.doFinal(data.getBytes("UTF8"));
			return base64en.encode(encryptedData);
		} catch (Exception e) {
			System.err.println("DES CBC算法，加密数据出错!");
			e.printStackTrace();
		} finally {
			base64en = null;
		}
		return null;
	}

	/**
	 * CBC解密函数
	 * 
	 * @param data
	 * @return
	 */
	public String CBCDecrypt(String data) {
		BASE64Decoder base64de = new BASE64Decoder();
		try {
			// 从原始密匙数据创建一个DESKeySpec对象
			DESKeySpec dks = new DESKeySpec(byKey);
			// 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
			// 一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);
			// using DES in CBC mode
			Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
			// 若采用NoPadding模式，data长度必须是8的倍数
			// Cipher cipher = Cipher.getInstance("DES/CBC/NoPadding");
			// 用密匙初始化Cipher对象
			IvParameterSpec param = new IvParameterSpec(byIV);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, param);
			// 正式执行解密操作
			byte decryptedData[] = cipher.doFinal(base64de.decodeBuffer(data));
			return new String(decryptedData, "UTF8");
		} catch (Exception e) {
			System.err.println("DES CBC算法，解密出错。");
			e.printStackTrace();
		} finally {
			base64de = null;
		}
		return null;
	}

	/**
	 * 哈希(SHA1)并CBC加密
	 * 
	 * @param data
	 * @return
	 */
	public String HashAndEncode(String data) {
		MessageDigest md = null;
		String strDes = null;

		try {
			byte[] bt = data.getBytes("UTF8");
			md = MessageDigest.getInstance(HASH_ALGORITHM);
			md.update(bt);
			strDes = CBCEncrypt(bytes2Hex(md.digest())); // to HexString
		} catch (Exception e) {
			System.err.println("散列值，加密出错。");
			return null;
		}
		return strDes;
	}

	private String bytes2Hex(byte[] bts) {
		String des = "";
		String tmp = null;
		for (int i = 0; i < bts.length; i++) {
			tmp = (Integer.toHexString(bts[i] & 0xFF)).toUpperCase();
			if (tmp.length() == 1) {
				des += "0";
			}
			des += tmp;
		}
		return des;
	}

	private void getKeyAndIV() {
		String KEY_64 = shareKey;
		String IV_64 = shareKey.substring(3) + shareKey.substring(0, 3);
		byte[] byKey0;
		byte[] byIV0;
		try {
			byKey0 = KEY_64.getBytes("UTF8");
			byIV0 = IV_64.getBytes("UTF8");
			byKey = fixEightBytes(byKey0);
			byIV = fixEightBytes(byIV0);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private byte[] fixEightBytes(byte[] bytes) {
		if (bytes.length == 8)
			return bytes;

		byte[] tmpBytes = new byte[8];
		int j = 0;
		for (int i = 0; i < 8; i++) {
			if (j >= bytes.length)
				j = 0;
			tmpBytes[i] = bytes[j++];
		}
		return tmpBytes;
	}

	public String getShareKey() {
		return shareKey;
	}

	public void setShareKey(String shareKey) throws Exception {
		if (shareKey == null || shareKey.length() < 4)
			throw new Exception("共享密钥必须4字符以上！");
		this.shareKey = shareKey;
		getKeyAndIV();
	}

}
