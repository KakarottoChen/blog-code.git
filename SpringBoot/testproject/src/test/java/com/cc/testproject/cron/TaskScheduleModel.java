package com.cc.testproject.cron;

import lombok.Data;
import java.util.List;

/**
 * @Description
 * @Author CC
 * @Date 2021/10/22
 **/
@Data
public class TaskScheduleModel {

    /**所选作业类型:
     * 1  -> 每秒
     * 2  -> 每分
     * 3  -> 每时
     * 4  -> 每天
     * 5  -> 每周
     * 6  -> 每月
     */
    Integer jobType;
    /**
     * 一周的哪几天
     */
    List<Integer> dayOfWeeks;
    /**
     * 一个月的哪几天
     */
    List<Integer> dayOfMonths;
    /**
     * 秒
     */
    Integer second;
    /**
     * 分
     */
    Integer minute;
    /**
     * 时
     */
    Integer hour;

}