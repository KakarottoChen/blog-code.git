/*
package com.cc.testproject.activiti7;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import com.cc.testproject.activiti7.pojo.Evection;
import com.cc.testproject.activiti7.securityConfigUtil.SecurityUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.model.payloads.ClaimTaskPayload;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

*/
/** activiti 与 springboot 整合
 * @Description
 * @Author CC
 * @Date 2021/12/30
 * @Version 1.0
 *//*

@Slf4j
public class SpringBootAndActiviti7 extends BaseTest {

    @Autowired
    private ProcessRuntime processRuntime;
    @Autowired
    private TaskRuntime taskRuntime;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private RepositoryService repositoryService;

    */
/** 自动部署流程 + 查询流程部署
     **//*

    @Test
    public void test01()throws Exception{
        securityUtil.logInAs("zhangsan");
        Page<ProcessDefinition> definitionPage = processRuntime.processDefinitions(Pageable.of(0, 100));
        List<ProcessDefinition> processDefinitions = definitionPage.getContent();
        log.info("可用的流程定义总数{}",processDefinitions.size());
        processDefinitions.forEach(processDefinition -> {
            log.info("id：{}",processDefinition.getId());
            log.info("ProcessDefinitionKey流程定义key：{}",processDefinition.getKey());
            log.info("name：{}",processDefinition.getName());
            log.info("ver：{}",processDefinition.getVersion());
            log.info("desc：{}",processDefinition.getDescription());
        });
    }

    */
/** zip方式部署
     **//*

    @Test
    public void test011()throws Exception{
        securityUtil.logInAs("zhangsan");
        File file = new File("E:\\itwen-dang2\\SpringBoot\\testcode\\testproject\\src\\" +
                "main\\resources\\processes\\myInclusiveGateway.zip");
        InputStream inputStream = new FileInputStream(file);
        ZipInputStream zipInput = new ZipInputStream(inputStream);
        Deployment deployment = repositoryService
                .createDeployment()
                .name("请假流程Cc")
                .addZipInputStream(zipInput)
                .deploy();
        log.info("id：{}",deployment.getId());
        log.info("name：{}",deployment.getName());
        log.info("key：{}",deployment.getKey());
        log.info("getCategory：{}",deployment.getCategory());
        log.info("getTenantId：{}",deployment.getTenantId());
        log.info("getDeploymentTime：{}",deployment.getDeploymentTime());
        zipInput.close();
    }

    */
/**
     * 指定文件夹部署
     *//*

    @Test
    public void test012()throws Exception{
        securityUtil.logInAs("zhangsan");
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("activitiOrSpringBoot/myEvectionInc.bpmn") // 添加bpmn资源
//                .addClasspathResource("activitiOrSpringBoot/myEvectionInc.bpmn20.png") // 添加png资源
                .name("出差流程CcC")
                .deploy();
        log.info("id：{}",deployment.getId());
        log.info("name：{}",deployment.getName());
        log.info("key：{}",deployment.getKey());
        log.info("getCategory：{}",deployment.getCategory());
        log.info("getTenantId：{}",deployment.getTenantId());
        log.info("getDeploymentTime：{}",deployment.getDeploymentTime());
    }

    */
/** 启动流程
     **//*

    @Test
    public void test02()throws Exception{
        securityUtil.logInAs("zhangsan");
        //通过DefinitionKey启动流程
        StartProcessPayload startProcessPayload = ProcessPayloadBuilder
                .start()
                .withProcessDefinitionKey("myEvectionInc")
                .build();
        ProcessInstance processInstance = processRuntime.start(startProcessPayload);
        log.info("流程实例ID：{}",processInstance.getId());
        log.info("getName：{}",processInstance.getName());
        log.info("getProcessDefinitionKey：{}",processInstance.getProcessDefinitionKey());
        log.info("getStartDate：{}",processInstance.getStartDate());
        log.info("getStatus：{}",processInstance.getStatus());
    }

    */
/** 查询任务
     **//*

    @Test
    public void test04()throws Exception{
        String user = "zhangsan";
        securityUtil.logInAs(user);
        Page<Task> taskPage = taskRuntime.tasks(Pageable.of(0, 10));
        List<Task> tasks = taskPage.getContent();
        log.info("{}，需要处理的任务条数：{}",user,tasks.size());
        tasks.forEach(task -> {
            log.info("任务id：{}",task.getId());
            log.info("名字：{}",task.getName());
            log.info("状态：{}",task.getStatus());
            log.info("负责人：{}",task.getAssignee());
            log.info("描述：{}",task.getDescription());
            log.info("Owner：{}",task.getOwner());
            log.info("ParentTaskId：{}",task.getParentTaskId());
            log.info("Priority：{}",task.getPriority());
        });
    }

    */
/** 拾取任务
     *    拾取任务后，该任务负责人才能是当前用户，然后才能完成任务
     *    拾取任务后，不能再拾取，需要先释放任务才能被其他人拾取
     **//*

    @Test
    public void test03()throws Exception{
        String user = "zhangsan";
        securityUtil.logInAs(user);
        Page<Task> taskPage = taskRuntime.tasks(Pageable.of(0, 100));
        List<Task> tasks = taskPage.getContent();
        log.info("{}， 需要处理的任务条数：{}",user,tasks.size());
        tasks.forEach(task -> {
            //拾取任务
            ClaimTaskPayload claimTaskPayload = TaskPayloadBuilder.claim().withTaskId(task.getId()).build();
            Task taskClaim = taskRuntime.claim(claimTaskPayload);
            //输出任务
            log.info("拾取的任务：任务id：{}",taskClaim.getId());
            log.info("拾取的任务：名字：{}",taskClaim.getName());
            log.info("拾取的任务：状态：{}",taskClaim.getStatus());
            log.info("拾取的任务：负责人：{}",taskClaim.getAssignee());
            log.info("拾取的任务：描述：{}",taskClaim.getDescription());
            log.info("拾取的任务：Owner：{}",taskClaim.getOwner());
            log.info("拾取的任务：ParentTaskId：{}",taskClaim.getParentTaskId());
            log.info("拾取的任务：Priority：{}",taskClaim.getPriority());
        });
    }

    */
/** 拾取任务 + 完成任务
     *//*

    @Test
    public void test05()throws Exception{
        String user = "zongjingli";
        securityUtil.logInAs(user);
        Page<Task> taskPage = taskRuntime.tasks(Pageable.of(0, 100));
        List<Task> tasks = taskPage.getContent();
        System.out.println(StrUtil.format("{}，需要处理的任务条数：{}",user,tasks.size()));
        tasks.forEach(task -> {
            System.out.println("----------------------------------------------------------------------------");
            //拾取任务
            ClaimTaskPayload claimTaskPayload = TaskPayloadBuilder
                    .claim()
                    .withTaskId(task.getId())
                    .build();
            Task taskClaim = taskRuntime.claim(claimTaskPayload);
            System.out.println("============1=============");
            //输出任务
            System.out.println(StrUtil.format("拾取的任务：任务id：{}",taskClaim.getId()));
            System.out.println(StrUtil.format("拾取的任务：名字：{}",taskClaim.getName()));
            System.out.println(StrUtil.format("拾取的任务：状态：{}",taskClaim.getStatus()));
            System.out.println(StrUtil.format("拾取的任务：负责人：{}",taskClaim.getAssignee()));
            System.out.println(StrUtil.format("拾取的任务：描述：{}",taskClaim.getDescription()));
            System.out.println(StrUtil.format("拾取的任务：Owner：{}",taskClaim.getOwner()));
            System.out.println(StrUtil.format("拾取的任务：ParentTaskId：{}",taskClaim.getParentTaskId()));
            System.out.println(StrUtil.format("拾取的任务：Priority：{}",taskClaim.getPriority()));
            //设置请假天数
            Map<String,Object> map = Maps.newHashMap();
            map.put("evection",new Evection(3d));
            //完成任务
            CompleteTaskPayload completeTaskPayload = TaskPayloadBuilder
                    .complete()
                    .withVariables(map)
                    .withTaskId(task.getId())
                    .build();
            Task taskFinish = taskRuntime.complete(completeTaskPayload);
            System.out.println("============2=============");
            //输出完成的任务
            System.out.println(StrUtil.format("拾取的任务：任务id：{}",taskFinish.getId()));
            System.out.println(StrUtil.format("拾取的任务：名字：{}",taskFinish.getName()));
            System.out.println(StrUtil.format("拾取的任务：状态：{}",taskFinish.getStatus()));
            System.out.println(StrUtil.format("拾取的任务：负责人：{}",taskFinish.getAssignee()));
            System.out.println(StrUtil.format("拾取的任务：描述：{}",taskFinish.getDescription()));
            System.out.println(StrUtil.format("拾取的任务：Owner：{}",taskFinish.getOwner()));
            System.out.println(StrUtil.format("拾取的任务：ParentTaskId：{}",taskFinish.getParentTaskId()));
            System.out.println(StrUtil.format("拾取的任务：Priority：{}",taskFinish.getPriority()));
        });
    }

}
*/
