package com.cc.testproject.thread;

import cn.hutool.core.date.DateUtil;
import com.cc.testproject.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/** 创建线程
 */
@Slf4j
public class ThreadTest extends BaseTest {

    /**
     * 程序睡
     */
    @Test
    public void test()throws Exception{
        try {
            long start = System.currentTimeMillis();
            log.info("开始时间：{}", DateUtil.date(start));
            Thread.sleep(5000);
            log.info("耗时：{}s", (System.currentTimeMillis() - start)/1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //一 直接创建线程------------------------------------------
    @Test
    public void test02()throws Exception{
        //--------1-Thread---------
        Thread thread = new Thread(()->{
            System.out.println("Thread"+1);
            System.out.println("Thread"+2);
        });
        //启动 Thread
        thread.start();
        //按照顺序执行
        thread.join();

        //--------2-Runnable---------
        Runnable runnable = () -> {
            System.out.println("Runnable"+3);
            System.out.println("Runnable"+4);
        };
        //启动 Runnable
        runnable.run();

        //--------3-Callable---------
        Callable<Integer> callable = () -> {
            System.out.println("Callable"+5);
            System.out.println("Callable"+6);
            return 5*6;
        };
        Integer call = callable.call();
        System.out.println("Callable返回值："+call);
    }

    //三 调用类实现的线程------------------------------------------
    @Test
    public void test03()throws Exception{
        //运行Thread
        Thread1 thread1 = new Thread1();
        thread1.start();
        System.out.println(thread1.getName());
        //运行Runable
        Runnable2 runnable2 = new Runnable2();
        runnable2.run();
        //运行Callable
        Callable3 callable3 = new Callable3();
        String call = callable3.call();
        System.out.println(call);
    }
    //使用类创建线程
    class Thread1 extends Thread{
        //线程具体做的事情
        @Override
        public void run() {
            System.out.println("Thread:类实现");
        }
    }
    //--------2-Runnable---------
    class Runnable2 implements Runnable{
        //线程具体做的事情
        @Override
        public void run() {
            System.out.println("Runnable:类实现");
        }
    }
    //--------3-Callable---------
    class Callable3 implements Callable{
        //线程具体做的事情
        @Override
        public String call() throws Exception {
            System.out.println("Callable:类实现");
            return "Callable";
        }
    }

}
