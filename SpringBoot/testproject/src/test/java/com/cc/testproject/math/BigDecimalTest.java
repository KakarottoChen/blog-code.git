package com.cc.testproject.math;

import com.cc.testproject.BaseTest;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/10 9:55
 * @Version 1.0
 */
public class BigDecimalTest extends BaseTest {

    /** 保留两位小数
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/10 13:11
     **/
    @Test
    public void test01()throws Exception{
        String str = "4.399999999999999";
        BigDecimal bg = new BigDecimal(str);
        Double doubleValue = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println(doubleValue);
    }

    @Test
    public void test02()throws Exception{
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Long.MAX_VALUE);
        System.out.println(new BigDecimal("92233720368547758071111"));
        System.out.println(new BigDecimal("0000000000092233720368547758071111"));
        System.out.println(BigDecimal.ONE);
        System.out.println(Long.valueOf("9223372036854775807"));

        System.out.println("0098");
        System.out.println("99");

        System.out.println("099");
        System.out.println("100");

        System.out.println("0999");
        System.out.println("1000");

        System.out.println("99");
        System.out.println("100");



    }




}
