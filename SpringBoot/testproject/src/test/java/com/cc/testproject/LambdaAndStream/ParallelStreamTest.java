package com.cc.testproject.LambdaAndStream;

import com.cc.testproject.BaseTest;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**parallelStream：多线程、并行流
 *  1、parallelStream背后的男人:ForkJoinPool 通用线程池
 *  2、ForkJoin框架是从jdk7中新特性,它同ThreadPoolExecutor一样，也实现了Executor和ExecutorService接口。
 *      1)它使用了一个无限队列来保存需要执行的任务，而线程的数量则是通过构造函数传入，如果没有向构造函数中传入希望的线程数量，
 *        那么当前计算机可用的CPU数量会被设置为线程数量作为默认值。
 *      2)正如我们上面那个列子的情况分析得知,lambda的执行并不是瞬间完成的,所有使用parallel streams的程序都有可能成为阻塞程序的源头,
 *        并且在执行过程中程序中的其他部分将无法访问这些workers,
 *        这意味着任何依赖parallel streams的程序在什么别的东西占用着common ForkJoinPool时将会变得不可预知并且暗藏危机.
 *  3、2.2)的意思是：使用 parallel streams 并行流，可能会把系统资源占用完，
 *        第二个使用 parallel streams 并行流的地方，可能就会没有线程使用，所以就会变很慢。
 *  4、怎么正确使用parallelStream
 *      1)是否需要并行？
 *        当数据量不大时，顺序执行往往比并行执行更快
 *      2)任务之间是否是独立的？是否会引起任何竞态条件？
 *        如果任务之间是独立的，并且代码中不涉及到对同一个对象的某个状态或者某个变量的更新操作，那么就表明代码是可以被并行化的。
 *      3)结果是否取决于任务的调用顺序？
 *        由于在并行环境中任务的执行顺序是不确定的，因此对于依赖于顺序的任务而言，并行化也许不能给出正确的结果。
 *   5、特点：
 *      1)parallelStram是采用多线程并行的方式运行
 *          parallelStream().forEach方式耗时最短
 *      2)ParallelStream().forEachOrdered是在多线程的基础上，保证了数据的顺序输出。
 *          优势在于很好的利用了CPU多核的资源。
 *   6、提升parallelStream性能 —— 使用自定义 ForkJoinPool 线程池
 */
public class ParallelStreamTest extends BaseTest {


    /**
     * 自定义通用线程池 ForkJoinPool
     */
    @Test
    public void test01()throws Exception{
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        ForkJoinPool forkJoinPool = new ForkJoinPool(8);
        List<Integer> join = forkJoinPool.submit(() ->
                list.parallelStream()
                    .map(integer -> integer + 100)
                    .collect(Collectors.toList())
        ).fork().join();
        System.out.println(join);

    }

    /** 并行执行、顺序执行
     **/
    @Test
    public void test02()throws Exception{
        //多线程：并行执行
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        numbers.parallelStream().forEach(System.out::print);
        System.out.println();
        System.out.println("--------------------------------------------------------");
        //多线程：顺序执行
        numbers.parallelStream().forEachOrdered(System.out::print);
    }

    /** 这是一个用来让你更加熟悉parallelStream的原理的实例
     **/
    @Test
    public void test03()throws Exception{
        System.out.println("Hello World!");
        // 构造一个10000个元素的集合
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(i);
        }
        // 统计并行执行list的线程
        Set<Thread> threadSet = new CopyOnWriteArraySet<>();
        // 并行执行
        list.parallelStream().forEach(integer -> {
            Thread thread = Thread.currentThread();
            // System.out.println(thread);
            // 统计并行执行list的线程
            threadSet.add(thread);
        });
        System.out.println("threadSet一共有" + threadSet.size() + "个线程");
        System.out.println("系统一个有"+Runtime.getRuntime().availableProcessors()+"个cpu");
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            list1.add(i);
            list2.add(i);
        }
        Set<Thread> threadSetTwo = new CopyOnWriteArraySet<>();
        CountDownLatch countDownLatch = new CountDownLatch(2);
        Thread threadA = new Thread(() -> {
            list1.parallelStream().forEach(integer -> {
                Thread thread = Thread.currentThread();
                // System.out.println("list1" + thread);
                threadSetTwo.add(thread);
            });
            countDownLatch.countDown();
        });
        Thread threadB = new Thread(() -> {
            list2.parallelStream().forEach(integer -> {
                Thread thread = Thread.currentThread();
                // System.out.println("list2" + thread);
                threadSetTwo.add(thread);
            });
            countDownLatch.countDown();
        });

        threadA.start();
        threadB.start();
        countDownLatch.await();
        System.out.print("threadSetTwo一共有" + threadSetTwo.size() + "个线程");

        System.out.println("---------------------------");
        System.out.println(threadSet);
        System.out.println(threadSetTwo);
        System.out.println("---------------------------");
        threadSetTwo.addAll(threadSet);
        System.out.println(threadSetTwo);
        System.out.println("threadSetTwo一共有" + threadSetTwo.size() + "个线程");
        System.out.println("系统一个有"+Runtime.getRuntime().availableProcessors()+"个cpu");
    }


}
