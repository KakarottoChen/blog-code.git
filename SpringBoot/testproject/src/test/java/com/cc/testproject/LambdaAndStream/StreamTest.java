package com.cc.testproject.LambdaAndStream;

import com.cc.testproject.BaseTest;
import com.cc.testproject.entity.User;
import com.cc.testproject.utils.StreamUtils;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/7 11:24
 * @Version 1.0
 */
@Slf4j
public class StreamTest extends BaseTest {

    @Resource
    private StreamUtils<User> streamUtils;

    @Test
    public void test0001()throws Exception{
        User user1 = new User();
        user1.setName("第一个");
        User user2 = new User();
        user2.setName("第二个");
        User user3 = new User();
        user3.setName("第三个");

        List<User> vos = Arrays.asList(user1,user2,user3);

        List<User> users = streamUtils.pageSkipLimit(vos, 1, 1);
        System.out.println("分页后的数据：" + users);
    }

    /** 获取满足条件的一个对象
     * @Description
     *  主要是：.findAny().orElse(null);
     *      orElse()里面还可以设置 WaterInfoThreeVo 的默认值：如果条件都不满足，就使用默认值。
     * @Author ChenCheng
     * @Date 2021/8/7 11:25
     **/
    @Test
    public void test01()throws Exception{
        /*WaterInfoThreeVo witVo = threeList.parallelStream()
                .filter(three -> three.getWaterMeterNo().equals(nbhao))
                .findAny().orElse(null);*/
    }

    /** stream和
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/10 13:13
     **/
    @Test
    public void test02()throws Exception{
        List<Integer> lists = Lists.newArrayList();
        for (int i = 0; i < 20000000; i++) {
            lists.add(i);
        }
        //并行流：执行效率很快，多线程操作，有可能出现线程安全问题；
        // 集合多，对线程安全无要求的，使用这个——（如果集合少，执行还会比stream慢）
        Stream<Integer> parallelStream = lists.parallelStream();
        Long stratTime1 = System.currentTimeMillis();
        parallelStream.forEach(integer -> {

        });
        System.out.println("parallelStream流：" + (System.currentTimeMillis() - stratTime1));
        //常规流：执行效率一般，单线程，无线程安全问题；
        // 集合少，对线程安全有要求的，使用这个——（适用于集合少）
        Long stratTime2 = System.currentTimeMillis();
        Stream<Integer> stream = lists.stream();
        stream.forEach(integer -> {

        });
        System.out.println("stream流：" + (System.currentTimeMillis() - stratTime2));
    }

    @Test
    public void test03()throws Exception{
        List<Integer> lists = Arrays.asList(1,2,3);
        List<Integer> collect = lists.stream().filter(i -> i > 4).collect(Collectors.toList());
        System.out.println(collect);
    }


}
