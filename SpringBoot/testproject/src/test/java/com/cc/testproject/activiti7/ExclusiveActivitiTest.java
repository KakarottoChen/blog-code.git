/*
package com.cc.testproject.activiti7;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import com.cc.testproject.activiti7.pojo.Evection;
import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

*/
/** 排他网关测试
 * @Description
 * @Author CC
 * @Date 2021/12/30
 * @Version 1.0
 *//*

public class ExclusiveActivitiTest extends BaseTest {

    */
/**
     * 流程部署
     * @throws Exception
     *//*

    @Test
    public void test01()throws Exception{
        //1、创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //2、得到RepositoryService实例 :流程定义和部署存储库的访问的服务
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //3、使用RepositoryService进行部署
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("bpmn/myExclusive.bpmn") // 添加bpmn资源
//                .addClasspathResource("bpmn/myEveitionCc.png") // 添加png资源
                .name("出差流程Cc排他网关")
                .deploy();
        //4、输出部署信息
        System.out.println(StrUtil.format("任务id：",deployment.getId()));
        System.out.println(StrUtil.format("任务名字：{}",deployment.getName()));
        System.out.println(StrUtil.format("租户id：{}",deployment.getTenantId()));
        System.out.println(StrUtil.format("部署时间：{}",deployment.getDeploymentTime()));
        System.out.println(StrUtil.format("类别：{}",deployment.getCategory()));
        System.out.println(StrUtil.format("key：{}",deployment.getKey()));
    }

    */
/**
     * 启动流程
     * @throws Exception
     *//*

    @Test
    public void test02()throws Exception{
        //        获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //        获取RunTimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //        流程定义key
        String key = "Process_13qo02j";
        //       创建变量集合
        Map<String, Object> map = new HashMap<>();
        //        创建出差pojo对象
        Evection evection = new Evection();
//        设置出差天数
        evection.setNum(3d);
//      定义流程变量，把出差pojo对象放入map
        map.put("evection",evection);
        //        启动流程实例，并设置流程变量的值（把map传入）
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(key, map);
        //      输出
        System.out.println("流程实例名称="+processInstance.getName());
        System.out.println("流程定义id=="+processInstance.getProcessDefinitionId());
    }

    */
/**
     * 完成任务
     * @throws Exception
     *//*

    @Test
    public void test03()throws Exception{
        //获取processEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        // 创建TaskService
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("Process_13qo02j")
                .singleResult();
        if (Objects.nonNull(task)){
            taskService.complete(task.getId());
            System.out.println("任务完成:"+ task.getId());
            System.out.println("任务完成:"+ task.getName());
        }
    }


}
*/
