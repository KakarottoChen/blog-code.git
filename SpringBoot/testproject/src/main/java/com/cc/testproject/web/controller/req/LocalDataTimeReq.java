package com.cc.testproject.web.controller.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author CC
 * @since 2022/3/16
 */
@Data
public class LocalDataTimeReq {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime localDateTime;

}
