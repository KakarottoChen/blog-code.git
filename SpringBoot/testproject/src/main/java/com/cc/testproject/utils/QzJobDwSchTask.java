package com.cc.testproject.utils;

import org.springframework.stereotype.Component;

import javax.sound.midi.Soundbank;
import java.time.LocalDateTime;
import java.util.concurrent.*;

/**
 * <p>存放队列</p>
 *
 * @author --
 * @since 2024/1/9
 */
public class QzJobDwSchTask {

    //有界阻塞队列——FIFO（先进先出）——必须指定大小——数组
    private static final ArrayBlockingQueue<String> QUEUE1 = new ArrayBlockingQueue<>(1);
    //有界阻塞队列——FIFO（先进先出）——默认大小：int最大值——链表
    private static final LinkedBlockingQueue <String> QUEUE2 = new LinkedBlockingQueue<>();
    //无界阻塞队列——FIFO（先进先出）——无限大——链表
    private static final LinkedTransferQueue <String> QUEUE3 = new LinkedTransferQueue<>();

    private static final PriorityBlockingQueue <String> QUEUE4 = new PriorityBlockingQueue<>();

    public void push(String req) throws InterruptedException {
        //添加元素1：成功返回true；失败抛异常 IllegalStateException
        boolean add = QUEUE1.add(req);
        //添加元素2：成功无；失败(队列满)：等待队列有空间再插入(一直阻塞到线程)
        QUEUE1.put(req);
        //添加元素3：成功返回true；失败(队列满)返回false。
        boolean offer1 = QUEUE1.offer(req);
        // 队列满了：可设置超时退出(阻塞时长：设置的时间)，放弃插入，返回false
        boolean offer2 = QUEUE1.offer(req, 5, TimeUnit.SECONDS);
    }

    public static void main(String[] args) throws InterruptedException {

        boolean offer = QUEUE1.offer("1");
        System.out.println("第1次添加：" + offer + "-时间：" + LocalDateTime.now());
        boolean offer1 = QUEUE1.offer("2");
        System.out.println("第2次添加：" + offer1 + "-时间：" + LocalDateTime.now());

        System.out.println(QUEUE1.take());

//        System.out.println(QUEUE1.element());
        System.out.println(QUEUE1.peek());
        System.out.println(QUEUE1);

//        System.out.println(QUEUE1.poll(5, TimeUnit.SECONDS));
//        System.out.println(QUEUE1.poll());
//        System.out.println(QUEUE1.remove());
//        System.out.println(QUEUE1.take());

//        System.out.println(QUEUE1.remove());
//        System.out.println(QUEUE1.remove());
//        System.out.println(QUEUE1.take());
//        System.out.println(QUEUE1.take());
//        System.out.println(QUEUE1.poll());
//        System.out.println(QUEUE1.poll(5, TimeUnit.SECONDS));

        //如队列满：等10秒，然后放弃插入，返回false
//        boolean offer = QUEUE1.offer("1", 10, TimeUnit.SECONDS);
//        System.out.println("第1次添加：" + offer + "-时间：" + LocalDateTime.now());
//        boolean offer1 = QUEUE1.offer("2", 10, TimeUnit.SECONDS);
//        System.out.println("第2次添加：" + offer1 + "-时间：" + LocalDateTime.now());
//
//        System.out.println("添加完成：" + QUEUE1);

        // 如队列满：抛异常
//        boolean add1 = QUEUE1.add("1");
//        System.out.println("第1次添加：" + add1);
//        boolean add2 = QUEUE1.add("2");
//        System.out.println("第2次添加：" + add2);

        //如队列满：一直等
//        QUEUE1.put("1");
//        System.out.println("第1次添加：" + LocalDateTime.now());
//        QUEUE1.put("2");
//        System.out.println("第2次添加：" + LocalDateTime.now());
    }

}
