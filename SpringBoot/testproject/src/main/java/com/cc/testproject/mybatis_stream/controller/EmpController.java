package com.cc.testproject.mybatis_stream.controller;

import com.cc.testproject.mybatis_stream.domain.Emp;
import com.cc.testproject.mybatis_stream.mapper.EmpMapper;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
@RestController
@RequestMapping("emp")
public class EmpController {

    @Resource
    private EmpMapper empMapper;
    @Resource
    private SqlSessionFactory sqlSessionFactory;

    @GetMapping
//    @Transactional(rollbackFor = Exception.class)
    public Object getEmp(){
        long start = System.currentTimeMillis();
        SqlSession sqlSession = sqlSessionFactory.openSession();

        Cursor<Emp> scan = sqlSession.getMapper(EmpMapper.class).scan(10000000);
        scan.forEach(emp -> {
            if (scan.isOpen()){
                System.out.println(emp);
            }
        });
        if (scan.isConsumed()){
            System.out.println("取完！！！！");
        }
        System.out.println("取出的数据条数：" + scan.getCurrentIndex());

        return "完成:" + (System.currentTimeMillis() - start);
    }

}
