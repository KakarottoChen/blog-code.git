package com.cc.testproject.springScheduled.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/** 定时任务配置类
 * @author CC
 * @since 2021/11/2
 **/
@Configuration
public class SchedulingConfig {

    /**
     * 定时任务配置
     * @return
     */
    @Bean
    public TaskScheduler taskScheduler() {
        // 任务调度线程池
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        // 定时任务执行线程池核心线程数：可同事执行4个任务
        taskScheduler.setPoolSize(4);
        taskScheduler.setRemoveOnCancelPolicy(true);
        // 线程名称前缀
        taskScheduler.setThreadNamePrefix("TaskSchedulerThreadPool-");
        return taskScheduler;
    }
}
