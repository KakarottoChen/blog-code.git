package com.cc.testproject.utils;

import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/** List分页工具类
 * @author CC
 * @since 2022/2/16
 **/
@Component
public class StreamUtils<T> {

    /** stream分页公用方法（返回传入的List）
     * @param t T
     * @param page 当前页
     * @param pageTotal 每页总条数
     * @return List<T>
     **/
    public List<T> pageSkipLimit(List<T> t, Integer page, Integer pageTotal) {
        return t.stream()
                .skip((long) (page - 1) * pageTotal)
                .limit(pageTotal)
                .collect(Collectors.toList());
    }

    /** 获取分页后的pageInfo。（设置了vos，总条数）
     * @param finalVos   需要分页的vos
     * @param page       当前页
     * @param pageTotal  每页条数
     * @return com.github.pagehelper.PageInfo<T>
     * @since 2023/5/22 0022
     * @author CC
     **/
    public PageInfo<T> getPageInfo(List<T> finalVos, Integer page, Integer pageTotal) {
        PageInfo<T> of = new PageInfo<>();
        of.setTotal(finalVos.size());
        //手动分页：finalVos
        List<T> pageVos = this.pageSkipLimit(finalVos, page, pageTotal);
        of.setList(pageVos);
        return of;
    }


}