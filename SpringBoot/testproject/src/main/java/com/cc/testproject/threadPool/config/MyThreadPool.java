package com.cc.testproject.threadPool.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import java.util.concurrent.ThreadPoolExecutor;

/** 自定义线程池
 * @Description
 * @Author CC
 * @Date 2022/1/14
 **/
@Configuration
public class MyThreadPool {

    /** 核心线程数 */
    private final int CORE_SIZE = 8;
    /** 等待的时间（单位：s） */
    private final int AWAIT_SECONDS = 300;
    /** 队列最大等待数，注意：如果小了，会造成程序卡顿 */
    private final int QUEUE_MAX_CAPACITY = 3000;
    /** 保持活动时间(单位：s) */
    private final int KEEP_ALIVE_SECOND  =  3600;
    /** 线程名字前缀 */
    private final String THREAD_PREX = "Ccccc-";

    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程数，默认1
        executor.setCorePoolSize(CORE_SIZE);
        //最大线程容量，默认int最大值
        executor.setMaxPoolSize(2 * CORE_SIZE);
        //队列容量，默认int最大值
        executor.setQueueCapacity(QUEUE_MAX_CAPACITY);
        //线程名字前缀
        executor.setThreadNamePrefix(THREAD_PREX);
        //保持活动时间(单位：s)，默认60s
        executor.setKeepAliveSeconds(KEEP_ALIVE_SECOND);
        //停机等待任务执行：false：直接关闭正在执行的任务，然后关机；true：等待正在执行任务的线程完成任务，再关机
        executor.setWaitForTasksToCompleteOnShutdown(true);
        //设置等待终止秒数
        executor.setAwaitTerminationSeconds(AWAIT_SECONDS);
        //设置拒绝执行处理程序
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }

}