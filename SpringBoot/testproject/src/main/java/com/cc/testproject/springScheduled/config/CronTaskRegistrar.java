package com.cc.testproject.springScheduled.config;

import com.cc.testproject.springScheduled.task.ScheduledTask;
import com.cc.testproject.springScheduled.task.SchedulingRunnable;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/** 缓存定时任务注册类，用来增加、删除定时任务。
 * @author CC
 * @since 2021/11/2
 **/
@Component
public class CronTaskRegistrar implements DisposableBean {

    /**
     * 缓存任务
     */
    private final Map<Runnable, ScheduledTask> scheduledTasks = new ConcurrentHashMap<>(16);

    /**
     * 使用自定义的任务调度配置
     */
    @Resource(name = "taskScheduler")
    private TaskScheduler taskScheduler;

    public TaskScheduler getScheduler() {
        return this.taskScheduler;
    }

    /**
     * 新增定时任务
     * @param task
     * @param cronExpression
     */
    public void addCronTask(Runnable task, String cronExpression) {
        Set<Runnable> runnables = scheduledTasks.keySet();
        final Runnable[] key = {null};
        runnables.forEach(run -> {
            if (run instanceof SchedulingRunnable){
                SchedulingRunnable sr = (SchedulingRunnable) run;
                String beanName = sr.getBeanName();
                if ("q".equals(beanName)){
                    key[0] = run;
                }
            }
        });

        addCronTask(new CronTask(task, cronExpression));
    }

    public void addCronTask(CronTask cronTask) {
        if (cronTask != null) {
            Runnable task = cronTask.getRunnable();
            if (this.scheduledTasks.containsKey(task)) {
                removeCronTask(task);
            }
            //新增定时任务
            ScheduledTask scheduledTask = this.scheduleCronTask(cronTask);
            this.scheduledTasks.put(task, scheduledTask);
        }
    }

    /**
     * 移除定时任务
     * @param task
     */
    public void removeCronTask(Runnable task) {
        ScheduledTask scheduledTask = this.scheduledTasks.remove(task);
        if (scheduledTask != null){
            scheduledTask.cancel();
        }
    }

    /** 创建 ScheduledTask定时任务 以便后面关闭
     * @param cronTask
     * @return
     */
    public ScheduledTask scheduleCronTask(CronTask cronTask) {
        //定时任务
        ScheduledTask scheduledTask = new ScheduledTask();
        //开启定时任务的真正方法
        scheduledTask.future = this.taskScheduler.schedule(cronTask.getRunnable(), cronTask.getTrigger());
        return scheduledTask;
    }

    /** 销毁所有的定时任务
     *     DisposableBean是Spring框架中的一个接口，它定义了一个destroy()方法，
     *     用于在Bean销毁时执行清理工作。
     *     当一个Bean实现了DisposableBean接口时，
     *     Spring容器会在该Bean销毁时自动调用destroy()方法，
     *     以便进行一些清理工作，例如释放资源等。
     *     如果您的Bean需要在销毁时执行一些清理工作，
     *     那么实现DisposableBean接口是一个很好的选择。
     */
    @Override
    public void destroy() {
        //关闭定时任务
        for (ScheduledTask task : this.scheduledTasks.values()) {
            task.cancel();
        }
        //清空缓存
        this.scheduledTasks.clear();
    }
}
