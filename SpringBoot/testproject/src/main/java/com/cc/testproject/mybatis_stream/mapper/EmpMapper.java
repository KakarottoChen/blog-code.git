package com.cc.testproject.mybatis_stream.mapper;

import com.cc.testproject.mybatis_stream.domain.Emp;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.cursor.Cursor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
@Mapper
public interface EmpMapper {

    @Select("select * from emp limit #{limit}")
    Cursor<Emp> scan(@Param("limit") int limit);

//    @Insert("insert into emp (name) values (#{name})")
    Integer insertEmp(@Param("names") List<String> names);

    @Select("select * from emp")
    List<Emp> selectAll();

}
