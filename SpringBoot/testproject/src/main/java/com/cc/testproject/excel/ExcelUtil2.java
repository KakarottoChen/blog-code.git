package com.cc.testproject.excel;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.checkerframework.checker.units.qual.s;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

//import static java.lang.System.out;

/**
 * @Description 导出工具类2
 * @Author ChenCheng
 * @Date 2021/8/5 9:50
 * @Version 1.0
 */
public class ExcelUtil2<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil2.class);

    private static final String DISPOSITION_FORMAT = "attachment; filename=\"%s\"; filename*=utf-8''%s";

    /** 未编码文件名转Content-Disposition值
     * @param filename 未编码的文件名(包含文件后缀)
     * @return Content-Disposition值
     */
    private static String disposition(String filename) {
        String codedFilename = filename;
        try {
            if (StringUtils.isNotBlank(filename)) {
                codedFilename = java.net.URLEncoder.encode(filename, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("不支持的编码:", e);
        }
        return String.format(DISPOSITION_FORMAT, codedFilename, codedFilename);
    }

    /** 设置：文件名、sheet名、标题头
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/6 20:23
     * @Param [fileName, sheetName, title, lists, aClass, response]
     * @return java.lang.Boolean
     **/
    public static Boolean export(String fileName,
                                 String sheetName,
                                 String title,
                                 List lists,
                                 Class aClass,
                                 HttpServletResponse response) {
        ExportParams exportParams = new ExportParams(title,
                sheetName,
                ExcelType.XSSF);
        //生成workbook 并导出
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams,aClass,lists);
        DateTimeFormatter dtft = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        StringBuilder fileNameSB = new StringBuilder(fileName).append("-");
        fileNameSB.append(dtft.format(LocalDateTime.now())).append(".xlsx");
        try {
            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Access-Control-Expose-Headers", "Content-disposition");
            response.setHeader("Content-disposition",
                    "attachment;filename=" + URLEncoder.encode(fileNameSB.toString(),"UTF-8")+
                       ";filename*=utf-8''"+URLEncoder.encode(fileNameSB.toString(),"UTF-8"));
            response.setCharacterEncoding("UTF-8");
            OutputStream output = response.getOutputStream();
            workbook.write(output);
            workbook.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
//        out.close();
        return true;
    }

    /** 设置：文件名、标题头
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/6 20:23
     * @Param [fileName, title, lists, aClass, response]
     * @return java.lang.Boolean
     **/
    public static Boolean export(String fileName,
                                 String title,
                                 List lists,
                                 Class aClass,
                                 HttpServletResponse response) {
        ExportParams exportParams = new ExportParams(title,
                "sheet01",
                ExcelType.XSSF);
        //生成workbook 并导出
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams,aClass,lists);
        DateTimeFormatter dtft = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        StringBuilder fileNameSB = new StringBuilder(fileName).append("-");
        fileNameSB.append(dtft.format(LocalDateTime.now())).append(".xlsx");
        try {
            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Access-Control-Expose-Headers", "Content-disposition");
            response.setHeader("Content-disposition",disposition(fileNameSB.toString()));
            response.setCharacterEncoding("UTF-8");
            OutputStream output = response.getOutputStream();
            workbook.write(output);
            workbook.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
//        out.close();
        return true;
    }

    /** 设置：文件名
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/6 20:23
     * @Param [fileName, title, lists, aClass, response]
     * @return java.lang.Boolean
     **/
    public static Boolean export(String fileName,
                                 List lists,
                                 Class aClass,
                                 HttpServletResponse response) {
        ExportParams exportParams = new ExportParams();
        exportParams.setType(ExcelType.XSSF);
        exportParams.setSheetName("sheet01");
        //生成workbook 并导出
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams,aClass,lists);
        DateTimeFormatter dtft = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        StringBuilder fileNameSB = new StringBuilder(fileName).append("-");
        fileNameSB.append(dtft.format(LocalDateTime.now())).append(".xlsx");
        try {
            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Access-Control-Expose-Headers", "Content-disposition");
            response.setHeader("Content-disposition",
                    "attachment;filename=" + URLEncoder.encode(fileNameSB.toString(),"UTF-8")+
                            ";filename*=utf-8''"+URLEncoder.encode(fileNameSB.toString(),"UTF-8"));
            response.setCharacterEncoding("UTF-8");
            OutputStream output = response.getOutputStream();
            workbook.write(output);
            workbook.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
//        out.close();
        return true;
    }

    /** 设置：文件名
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/6 20:23
     * @Param [fileName, title, lists, aClass, response]
     * @return java.lang.Boolean
     **/
    public static Boolean exportHSSF(String fileName,
                                     List lists,
                                     Class aClass,
                                     HttpServletResponse response) {
        ExportParams exportParams = new ExportParams();
        exportParams.setSheetName("sheet01");
        exportParams.setType(ExcelType.HSSF);
        //生成workbook 并导出
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams,aClass,lists);
        DateTimeFormatter dtft = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        StringBuilder fileNameSB = new StringBuilder(fileName).append("-");
        fileNameSB.append(dtft.format(LocalDateTime.now())).append(".xls");
        try {
            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Access-Control-Expose-Headers", "Content-disposition");
            response.setHeader("Content-disposition",
                    "attachment;filename=" + URLEncoder.encode(fileNameSB.toString(),"UTF-8")+
                            ";filename*=utf-8''"+URLEncoder.encode(fileNameSB.toString(),"UTF-8"));
            response.setCharacterEncoding("UTF-8");
            OutputStream output = response.getOutputStream();
            workbook.write(output);
            workbook.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
//        out.close();
        return true;
    }

    /** 导入Excel文件
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/20 10:27
     * @Param [t, multipartFile]
     * @return java.util.List<org.apache.poi.ss.formula.functions.T>
     **/
    public List<T> impostExcel(T t,MultipartFile multipartFile) {
        //获得文件名
        String fileName = multipartFile.getOriginalFilename();
        //判断文件是否是excel文件
        if(!fileName.endsWith("xls") && !fileName.endsWith("xlsx")){
            try {
                throw new Exception(fileName + "——不是excel文件");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ImportParams params = new ImportParams();
        params.setHeadRows(1);
//        params.setConcurrentTask(true);
//        s file = FileUtil.transferToFile(multipartFile);
//        return ExcelImportUtil.importExcel(file,t.getClass(), params);
        return null;
    }

    /** 生成下拉选择
     * firstRow 開始行號 根据此项目，默认为2(下标0开始)
     * lastRow  根据此项目，默认为最大65535
     * firstCol 区域中第一个单元格的列号 (下标0开始)
     * lastCol 区域中最后一个单元格的列号
     * strings 下拉内容
     * */
    public static void selectList(Workbook workbook,int firstCol,int lastCol,String[] strings ){
        Sheet sheet = workbook.getSheetAt(0);
        //  生成下拉列表
        //  只对(x，x)单元格有效
        CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, 65535, firstCol, lastCol);
        //  生成下拉框内容
        DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(strings);
        HSSFDataValidation dataValidation = new HSSFDataValidation(cellRangeAddressList, dvConstraint);
        //  对sheet页生效
        sheet.addValidationData(dataValidation);
    }


    /**
     * 验证日期格式
     * @param workbook
     * @param firstCol
     * @param lastCol
     */
    public static void selectListDate(Workbook workbook,int firstCol,int lastCol){
        Sheet sheet = workbook.getSheetAt(0);
        //  生成下拉列表
        //  只对(x，x)单元格有效
        CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, 65535, firstCol, lastCol);
        DVConstraint constraint = DVConstraint.createDateConstraint(DVConstraint.OperatorType.BETWEEN, "1900/01/01", "2099/12/31", "yyyy/MM/dd");
        HSSFDataValidation dataValidate = new HSSFDataValidation(cellRangeAddressList, constraint);
        //错误提示信息
        dataValidate.createErrorBox("提示","请输入[yyyy/MM/dd]格式日期");
        //  对sheet页生效
        sheet.addValidationData(dataValidate);

    }
}