package com.cc.testproject.web.controller;

import com.cc.testproject.entity.ParamEntiry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/2 10:50
 * @Version 1.0
 */
@RequestMapping({"/start1","start2"})
@RestController
@Slf4j
public class StartController {

    /**
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/2 11:14
     **/
    @GetMapping("/get/{param1}/{param2}")
    public ParamEntiry getStart(@PathVariable String param1, @PathVariable String param2) {
        log.info("获取getStart成功，参数1是：{}，参数2是：{}",param1,param2);
        log.error("错误日志：参数1：{}。参数2：{}",param1,param2);
        return new ParamEntiry(param1, param2);
    }


}
