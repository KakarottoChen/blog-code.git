## 这个模块是：websocket的基础使用
### 一、基础
#### 1、依赖/js
```
        <!--webSocket-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-websocket</artifactId>
        </dependency>
        <!--模板的包 - resources\templates下的html才会生效 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>
```
JQuery下载：
```
1、在使用Http的URL访问html页面时，需要导入themeleaf
2、ajax使用需导入jQuery脚本
https://jquery.com/download/
生产版本：Download the compressed, production jQuery 3.5.1
开发版本：Download the uncompressed, development jQuery 3.5.1
```

#### 2、webSocket4个事件：

|  事件名称   |  作用   |备注 |
| --------- | --------| --------|
| onopen    | 创建连接时触发| |
| onclose   | 连接断开时触发| |
| onmessage | 接收到信息时触发| |
| onerror   | 通讯异常时触发| |

### 二、使用
#### 1、基础配置
配置bean：WebSocketConfig 必须配置<br/>
启动类：@EnableWebSocket

#### 2、后端向前端发送消息
GetWebSocketController<br/>
WebSocketController<br/>
MyWebSocket<br/>

socketIndex.html

#### 3、实现聊天室
聊天室功能。主要功能如下<br/>
　　1 用户在浏览器端进入聊天室(创建WebSocket连接)；<br/>
　　2 用户端发送消息到服务端(客户端像服务端发信息)；<br/>
　　3 服务端将消息转发到客户端(服务端向客户端发信息)；<br/>
　　4 用户退出聊天室(断开WebSocket连接)。

WebSocketUtil<br/>
ChatController<br/>

jquery-3.6.0.js<br/>
chat.html<br/>

#### 4、需要实现向特定用户发送消息



