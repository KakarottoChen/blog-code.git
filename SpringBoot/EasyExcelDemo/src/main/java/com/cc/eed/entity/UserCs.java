package com.cc.eed.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/16
 */
@Data
public class UserCs {

    @ExcelProperty(value = {"主键"}, order = 0)
    private String id;

    @ExcelProperty(value = {"合并", "名字"}, order = 1)
    private String name;

    @ExcelProperty(value = {"合并2", "年龄和性别", "年龄"}, order = 2)
    private Integer age;

    @ExcelProperty(value = {"合并2", "年龄和性别", "年龄和性别"}, order = 3)
    private Integer sex;

    @ExcelIgnore
    private Date date;



}
