package com.cc.eed.build;

import com.cc.eed.entity.UserCs;
import com.google.common.collect.Lists;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/16
 */
public class UserCsBuild {

    public static List<UserCs> getList(){

        List<UserCs> list = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            UserCs userCs = new UserCs();
            userCs.setId(String.valueOf(i));
            userCs.setName("名字：" + i);
            userCs.setAge(i + 10);
            userCs.setDate(new Date());
            userCs.setSex(i % 2 == 1 ? 0 : 1);
            list.add(userCs);
        }

        return list;
    }



}
