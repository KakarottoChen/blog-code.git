package com.cc.eed.web.controller;

import com.cc.eed.service.IUserCsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/16
 */
@RestController
@RequestMapping("/userCs")
public class UserCsController {

    @Resource
    private IUserCsService userCsService;

    @GetMapping
    public void get(HttpServletResponse response){
        userCsService.get(response);
    }


}
