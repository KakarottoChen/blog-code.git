package com.cc.wsd;

import com.cc.wsd.entity.YourRequestObject;
import com.cc.wsd.entity.YourResponseObject;
import com.cc.wsd.xml.GetDeviceCurrentData;
import com.cc.wsd.xml.GetDeviceCurrentDataResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.annotation.Resource;


/**
 * @author CC
 * @since 2023/5/26 0026
 */
@SpringBootTest
public class Test01Request {

    @Resource
    private WebServiceTemplate webServiceTemplate;

    @Test
    public void test03() throws Exception {
        String url = "http://dataservice.webService.service.mls.hac.com";
        GetDeviceCurrentData request = new GetDeviceCurrentData();
        //"yagr","62BC9utKW20=","",500,1
        request.setLoginName("yagr");
        request.setLoginPassword("62BC9utKW20=");
        request.setImei("");
        request.setPageSize(500);
        request.setPageIndex(1);

        GetDeviceCurrentDataResponse response = (GetDeviceCurrentDataResponse) webServiceTemplate.marshalSendAndReceive(url, request);
        System.out.println(response);
    }

    @Test
    public void test01() throws Exception {

        /*JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient("http://ws.webxml.com.cn/WebServices/WeatherWS.asmx?wsdl");
        // namespaceURI 是命名空间,在wsdl地址中，需要访问查看
        // methodName是方法名，我们要调用的方法
        QName name = new QName(
                "http://WebXml.com.cn/",
                "getRegionCountry");
        Object[] objects;
        try {
            // 第一个参数是上面的QName，第二个开始为参数，可变数组
            objects = client.invoke(name, "");
            System.out.println("对象地址是:"+objects);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    // *****用这种方式吧。*****
    @Test
    public void test02() throws Exception {
        /*JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient("http://47.92.223.245:7007/mls/webService/dataService?wsdl");
        // namespaceURI 是命名空间,在wsdl地址中，需要访问查看
        // methodName是方法名，我们要调用的方法
        QName name = new QName(
                "http://dataservice.webService.service.mls.hac.com",
                "getDeviceCurrentData");
        Object[] objects;
        try {
            // 第一个参数是上面的QName，第二个开始为参数，可变数组
            objects = client.invoke(name, "yagr","62BC9utKW20=","",500,1);
            System.out.println("对象地址是:"+objects);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }


}
