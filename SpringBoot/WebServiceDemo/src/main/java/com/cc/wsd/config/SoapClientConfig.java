//package com.cc.wsd.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.oxm.jaxb.Jaxb2Marshaller;
//
///**
// * @author QuS
// * @date 2022/3/17 14:57
// */
//@Configuration
//public class SoapClientConfig {
//
//    @Bean
//    public Jaxb2Marshaller marshaller() {
//        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//        marshaller.setContextPath("com.cc.wsd.xml");
//        return marshaller;
//    }
//
//    @Bean
//    public SOAPConnector soapConnector(Jaxb2Marshaller marshaller) {
//        SOAPConnector client = new SOAPConnector();
//        client.setDefaultUri("http://47.92.223.245:7007/mls/webService/dataService?wsdl");
//        client.setMarshaller(marshaller);
//        client.setUnmarshaller(marshaller);
//        return client;
//    }
//}
