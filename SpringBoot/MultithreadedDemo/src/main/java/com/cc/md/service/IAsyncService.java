package com.cc.md.service;

/**
 * @author CC
 * @since 2023/5/24 0024
 */
public interface IAsyncService {

    /** 异步方法1
     * @since 2023/5/24 0024
     * @author CC
     **/
    void async1();

    /** 异步方法2
     * @since 2023/5/24 0024
     * @author CC
     **/
    void async2();

}
