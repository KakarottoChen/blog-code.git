package com.cc.md.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/** CPU型的线程池
 * @author CC
 * @since 2023/5/23 0023
 */
@Configuration
public class CpuThreadPool {

    /** 线程数量
     * CUP数量：N = Runtime.getRuntime().availableProcessors()
     * IO密级：2 * N
     * CPU密级：1 + N
     */
    public static final int THREAD_SIZE = 1 + (Runtime.getRuntime().availableProcessors());
    /**
     * 队列大小
     */
    public static final int QUEUE_SIZE = 1000;

    @Bean(name = "myCpuThreadPool")
    public ThreadPoolTaskExecutor threadPoolExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(THREAD_SIZE);
        executor.setMaxPoolSize(THREAD_SIZE);
        executor.setQueueCapacity(QUEUE_SIZE);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        executor.setKeepAliveSeconds(60);
        executor.setAllowCoreThreadTimeOut(true);
        executor.setAwaitTerminationSeconds(300);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setThreadNamePrefix("myCpu-T-Pool-");
        executor.initialize();
        return executor;
    }




}
