package com.cc.md.entity;

import lombok.Data;

/**
 * @author CC
 * @since 2023/5/24 0024
 */
@Data
public class UserCs {

    private String name;

    private Integer age;

}
