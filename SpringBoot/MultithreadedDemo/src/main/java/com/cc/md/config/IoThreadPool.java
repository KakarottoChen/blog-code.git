package com.cc.md.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/** IO型的线程池
 * <li>IO密集型配置线程数经验值是：2N (CPU核数*2)</li>
 * <li>异步线程池：建议用io密集型：</li>
 *      对于异步线程池，通常建议使用IO密集型线程池。
 *      异步任务通常是网络IO或磁盘IO等操作，这些操作的执行时间相对于CPU计算的执行时间要长得多。
 *      使用IO密集型线程池可以更好地利用IO资源，提高多个异步任务的执行效率和吞吐量，
 *      同时避免由于过多的线程切换和上下文切换导致的性能损失。
 * @author CC
 * @since 2023/5/23 0023
 */
@Configuration
@EnableAsync
public class IoThreadPool {

    /** 线程数量
     * CUP数量：N = Runtime.getRuntime().availableProcessors()
     * IO密级：2 * N
     * CPU密级：1 + N
     */
    public static final int THREAD_SIZE = 2 * (Runtime.getRuntime().availableProcessors());
    /**
     * 队列大小
     */
    public static final int QUEUE_SIZE = 1000;

    @Bean(name = "myIoThreadPool")
    public ThreadPoolTaskExecutor threadPoolExecutor(){
        //配置线程池选择：ThreadPoolTaskExecutor，还是选择ThreadPoolExecutor好些？
        // -> ThreadPoolTaskExecutor 是 Spring 框架中对 Java 自带的线程池 ThreadPoolExecutor 进行了封装和扩展，
        //    并增加了一些优化和功能。通常来说，如果你使用 Spring 框架，需要使用线程池，那么建议使用 ThreadPoolTaskExecutor。
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 1核心线程数：当线程池中的线程数量为 corePoolSize 时，即使这些线程处于空闲状态，也不会销毁（除非设置 allowCoreThreadTimeOut=true）。
        //  -> 核心线程，也就是正在处理中的任务
        //  -> 虽然 CPU 核心数可以作为线程池中线程数量的参考指标，但最终线程数量还需要根据具体情况进行设置和调整。
        //  -> 如果同时运行的线程数量超过 CPU 核心数，就会发生--线程上下文切换--，导致额外的开销和性能下降。所以线程不能创建得过多
        executor.setCorePoolSize(THREAD_SIZE);
        // 2最大线程数：线程池中允许的线程数量的最大值。
        //  -> 当线程数 = maxPoolSize最大线程数时，还有新任务，就会放进队列中等待执行 ↓↓↓
        executor.setMaxPoolSize(THREAD_SIZE);
        // 3队列长度：当核心线程数达到最大时，新任务会放在队列中排队等待执行
        //  -> 根据业务配置，如果队列长度过大，可能会导致系统内存资源占用过高，最终导致 OOM，需要注意控制
        //  -> 如果需要执行的任务装满了队列，就会走拒绝策略 ↓↓↓
        executor.setQueueCapacity(QUEUE_SIZE);
        // 4拒绝策略(官方提供4种，也可以自定义)：因达到线程边界和任务队列满时，针对新任务的处理方法。
        // -> AbortPolicy：直接丢弃任务并抛出 RejectedExecutionException 异常。(默认策略)
        // -> DiscardPolicy：直接丢弃掉，不会抛出异常
        // -> DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务（重复此过程）
        // -> CallerRunsPolicy：交给主线程（调用线程）去执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        // 5空闲线程存活时间(默认60s)：设置当前线程池中空闲线程的存活时间，即线程池中的线程如果有一段时间没有任务可执行，则会被回收掉。
        //  -> 当线程池中的线程数大于 corePoolSize 时，多余的空闲线程将在销毁之前等待新任务的最长时间。
        //  -> 如果一个线程在空闲时间超过了 keepAliveSeconds，且当前线程池中线程数量大于 corePoolSize，则该线程将会被回收；
        //  -> 核心线程会一直存活，除非线程池被关闭 或 设置下面的参数
        //  -> 如果 AllowCoreThreadTimeout设置为true，核心线程也会被回收，直到线程池中的线程数降为 0。
        //     但如果线程池中有任务在执行，那么空闲线程就会一直保持存活状态，直到任务执行完毕。
        //  -> 该方法的使用可以将线程池的空闲线程回收，以减少资源占用，同时也能保证线程池中始终有可用的线程来执行任务，提高线程池的效率。
        executor.setKeepAliveSeconds(60);
        //6是否禁止线程池自动终止空闲的核心线程。
        // 为 true 时，空闲的核心线程会在 keepAliveTime 时间后被回收，并且在后续任务到来时需要重新创建线程来执行任务。
        // 为 false 时，线程池中的核心线程不会被回收，即使它们处于空闲状态一段时间。
        //  -> 在线程池创建时，就会预先创建核心线程数的线程，这些线程将一直存在，除非线程池被关闭或重新配置。
        executor.setAllowCoreThreadTimeOut(true);
        // 7当前线程池的等待时间：指等待所有任务执行完毕后线程池的最长时间。300秒 = 5分钟
        // -> 当所有任务执行完毕后，线程池会等待一段时间（即等待时间），来确保所有任务都已经完成。
        // -> 如果在等待时间内所有任务仍未完成，则线程池会强制停止，以确保任务不会无限制地执行下去。
        executor.setAwaitTerminationSeconds(300);
        // 8当前线程池是否在关闭时等待所有任务执行完成
        // -> 可以确保所有任务都执行完毕后才关闭线程池，避免任务被丢弃，同时也确保线程池可以正常结束，释放资源。
        // -> 为 true 时，线程池在关闭时会等待所有任务都执行完成后再关闭
        // -> 为 false 时，线程池会直接关闭，未执行完成的任务将被丢弃。
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 9线程前缀名称
        executor.setThreadNamePrefix("myIo-Th-Pool-");
        // 初始化
        executor.initialize();
        return executor;
    }



}
