package com.cc.md;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author cc
 */
@SpringBootApplication
public class MultithreadedDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultithreadedDemoApplication.class, args);
    }

}
